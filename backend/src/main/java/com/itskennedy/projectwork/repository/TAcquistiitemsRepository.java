package com.itskennedy.projectwork.repository;

import com.itskennedy.projectwork.dao.TAcquistiitemsDAO;
import com.itskennedy.projectwork.dao.TFattureDAO;
import com.itskennedy.projectwork.service.TAcquistiitemsService;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface TAcquistiitemsRepository extends CrudRepository<TAcquistiitemsDAO, Long> {
    List<TAcquistiitemsDAO> getAllByAcquistoID(Long acquistoId);
    void deleteAllByAcquistoID(Long acquistoId);
}
