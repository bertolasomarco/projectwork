package com.itskennedy.projectwork.repository;

import com.itskennedy.projectwork.dao.TFattureDAO;
import com.itskennedy.projectwork.dao.TItemsDAO;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.CrudRepository;

public interface TFattureRepository extends JpaRepository<TFattureDAO, Long>, JpaSpecificationExecutor<TFattureDAO> {
    TFattureDAO getFirstByNumeroFattura(String NumeroFattura);
    void deleteByNumeroFattura(String n);

}
