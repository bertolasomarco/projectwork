package com.itskennedy.projectwork.repository;

import com.itskennedy.projectwork.dao.TItemsDAO;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface TItemRepository extends JpaRepository<TItemsDAO, Long>, JpaSpecificationExecutor<TItemsDAO> {

    TItemsDAO getTItemsDAOByAsin(String asin);

    @Query("select item from TItemsDAO item where item.giacenza < item.sogliadisponibilita")
    List<TItemsDAO> getItemsARischio();

}
