package com.itskennedy.projectwork.repository;

import com.itskennedy.projectwork.dao.TOrderitemDAO;
import com.itskennedy.projectwork.dao.TOrdersDAO;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface TOrderitemRepository extends JpaRepository<TOrderitemDAO, Long> {
    List<TOrderitemDAO> getTOrderitemDAOSByAmazonOrderId(String id);
    List<TOrderitemDAO> getTOrderitemDAOSByAmazonOrderIdAndAsin(String id, String asin);
    @Query("SELECT oi from TOrderitemDAO  oi, TItemsDAO i where oi.amazonOrderId = ?1 and oi.asin = i.asin and i.categoria like ?2")
    List<TOrderitemDAO> getTOrderitemDAOSByAmazonOrderIdAndCategoria(String id, String categoria);
    List<TOrderitemDAO> getTOrderitemDAOSByAsin(String asin);
}
