package com.itskennedy.projectwork.repository;

import com.itskennedy.projectwork.dao.TUserDAO;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface TUserRepository extends JpaRepository<TUserDAO, Long> {
    Optional<TUserDAO> findByUsername(String username);
    Optional<TUserDAO> findByToken(String token);
}
