package com.itskennedy.projectwork.repository;

import com.itskennedy.projectwork.dao.TItemsDAO;
import com.itskennedy.projectwork.dao.TOrdersDAO;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.CrudRepository;

public interface TOrdersRepository extends JpaRepository<TOrdersDAO, String> , JpaSpecificationExecutor<TOrdersDAO> {
}
