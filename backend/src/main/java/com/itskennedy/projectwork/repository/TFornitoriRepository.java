package com.itskennedy.projectwork.repository;

import com.itskennedy.projectwork.dao.TFornitoriDAO;
import com.itskennedy.projectwork.dao.TItemsDAO;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.CrudRepository;

public interface TFornitoriRepository extends CrudRepository<TFornitoriDAO, Long>, JpaSpecificationExecutor<TFornitoriDAO> {
    TFornitoriDAO getFirstByFornitoriID(Long fornitoriID);
}
