package com.itskennedy.projectwork.dao;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.List;

@Data
public class OrdersCointainer {
    @JsonProperty(value = "Orders")
    TOrdersDAO[] orders;
}
