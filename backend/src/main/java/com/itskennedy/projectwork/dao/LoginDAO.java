package com.itskennedy.projectwork.dao;

import lombok.Data;

@Data
public class LoginDAO {
    String username;
    String password;
}
