package com.itskennedy.projectwork.dao;


import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "tusers")
@Data
@Getter
@Setter
public class TUserDAO {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    Long usersID;
    String username;
    String password;
    String token;

    public TUserDAO() {

    }
}
