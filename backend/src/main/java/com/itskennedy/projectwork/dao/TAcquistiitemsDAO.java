package com.itskennedy.projectwork.dao;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name="tacquistiitems")
public class TAcquistiitemsDAO {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="AcquistoItemID")
    public Long acquistoItemID;
    @Column(name="AcquistoID")
    public Long acquistoID;
//    @Column(name="ItemID")
//    public Long itemID;
    @Column(name="QuantitaAcquistata")
    public int qauantitaAcquistata;
    @Column(name="prezzoUnitarioAcquisto")
    public float PrezzoUnitarioAcquisto;

    @ManyToOne
    @JoinColumn(name="ItemID", nullable=false)
    private TItemsDAO items;

}
