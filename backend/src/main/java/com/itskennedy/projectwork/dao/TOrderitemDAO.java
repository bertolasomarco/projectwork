package com.itskennedy.projectwork.dao;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name="torderitem")
@JsonIgnoreProperties(value = { "Title","QuantityShipped","PointsGrantedPointsNumber","PointsGrantedPointsMonetaryValueCurrencyCode", "PointsGrantedPointsMonetaryValueAmount","ItemPriceCurrencyCode","ItemPriceAmount","ShippingPriceCurrencyCode","ShippingPriceAmount","PromotionIds" })
public class TOrderitemDAO
{
    @Id
    public Long OrderItemId;
    @Column(name = "amazonOrderId")
    @JsonProperty(value = "AmazonOrderId")
    public String amazonOrderId;
    @Column(name = "ASIN")
    @JsonProperty(value = "ASIN")
    public String asin;
    @JsonProperty(value = "QuantityOrdered")
    public int qt;
}