package com.itskennedy.projectwork.dao;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

@Data
@Entity
@Table(name = "torders")
public class TOrdersDAO {
    @Id
    @Column(name = "AmazonOrderId")
    @JsonProperty(value = "AmazonOrderId")
    public String amazonOrderId;
    @JsonFormat(shape=JsonFormat.Shape.STRING, pattern="yyyy-MM-dd hh:mm:ss.SS")
    @Column(name = "PurchaseDate")
    @JsonProperty(value = "PurchaseDate")
    public Date purchaseDate;
    @JsonFormat(shape=JsonFormat.Shape.STRING, pattern="yyyy-MM-dd hh:mm:ss.SS")
    @Column(name = "LastUpdateDate")
    @JsonProperty(value = "LastUpdateDate")
    public Date lastUpdateDate;
    @Column(name = "OrderStatus")
    @JsonProperty(value = "OrderStatus")
    public String orderStatus;
    @Column(name = "FulfillmentChannel")
    @JsonProperty(value = "FulfillmentChannel")
    public String fulfillmentChannel;
    @Column(name = "NumberOfItemsShipped")
    @JsonProperty(value = "NumberOfItemsShipped")
    public int numberOfItemsShipped;
    @Column(name = "NumberOfItemsUnshipped")
    @JsonProperty(value = "NumberOfItemsUnshipped")
    public int numberOfItemsUnshipped;
    @Column(name = "PaymentMethod")
    @JsonProperty(value = "PaymentMethod")
    public String paymentMethod;
    @Column(name = "PaymentMethodDetails")
    @JsonProperty(value = "PaymentMethodDetails")
    public String paymentMethodDetails;
    @Column(name = "MarketplaceId")
    @JsonProperty(value = "MarketplaceId")
    public String marketplaceId;
    @Column(name = "ShipmentServiceLevelCategory")
    @JsonProperty(value = "ShipmentServiceLevelCategory")
    public String shipmentServiceLevelCategory;
    @Column(name = "OrderType")
    @JsonProperty(value = "OrderType")
    public String orderType;
    @JsonFormat(shape=JsonFormat.Shape.STRING, pattern="dd-M-yyyy hh:mm:ss.SS")
    @Column(name = "EarliestShipDate")
    @JsonProperty(value = "EarliestShipDate")
    public Date earliestShipDate;
    @JsonFormat(shape=JsonFormat.Shape.STRING, pattern="dd-M-yyyy hh:mm:ss.SS")
    @Column(name = "LatestShipDate")
    @JsonProperty(value = "LatestShipDate")
    public Date latestShipDate;
    @Column(name = "IsBusinessOrder")
    @JsonProperty(value = "IsBusinessOrder")
    public boolean isBusinessOrder;
    @Column(name = "IsPrime")
    @JsonProperty(value = "IsPrime")
    public boolean isPrime;
    @Column(name = "IsGlobalExpressEnabled")
    @JsonProperty(value = "IsGlobalExpressEnabled")
    public boolean isGlobalExpressEnabled;
    @Column(name = "IsPremiumOrder")
    @JsonProperty(value = "IsPremiumOrder")
    public boolean isPremiumOrder;
    @Column(name = "IsSoldByAB")
    @JsonProperty(value = "IsSoldByAB")
    public boolean isSoldByAB;
    @Column(name = "CompanyLegalName")
    @JsonProperty(value = "CompanyLegalName")
    public String companyLegalName;
    @Column(name = "BuyerEmail")
    @JsonProperty(value = "BuyerEmail")
    public String buyerEmail;
    @Column(name = "BuyerName")
    @JsonProperty(value = "BuyerName")
    public String buyerName;
    @Column(name = "PurchaseOrderNumber")
    @JsonProperty(value = "PurchaseOrderNumber")
    public String purchaseOrderNumber;
    @Column(name = "ShippingAddressName")
    @JsonProperty(value = "ShippingAddressName")
    public String shippingAddressName;
    @Column(name = "ShippingAddressLine1")
    @JsonProperty(value = "ShippingAddressLine1")
    public String shippingAddressLine1;
    @Column(name = "ShippingAddressCity")
    @JsonProperty(value = "ShippingAddressCity")
    public String shippingAddressCity;
    @Column(name = "ShippingCityStateOrRegion")
    @JsonProperty(value = "ShippingCityStateOrRegion")
    public String shippingCityStateOrRegion;
    @Column(name = "ShippingStateOrRegionPostalCode")
    @JsonProperty(value = "ShippingStateOrRegionPostalCode")
    public String shippingStateOrRegionPostalCode;
}
