package com.itskennedy.projectwork.dao;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@NoArgsConstructor
@Table(name = "tfatture")
@Data
public class TFattureDAO
{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "AcquistoID")
    public Long acquistoID;
    @Column(name = "DataFattura")
    @JsonFormat(shape=JsonFormat.Shape.STRING, pattern="yyyy-MM-dd'T'hh:mm:ss.SSS'Z'")
    public java.sql.Date dataFattura;
    @Column(name = "NumeroFattura")
    public String numeroFattura;
    @Column(name = "Effettuata")
    public boolean effettuata;

    @ManyToOne
    @JoinColumn(name="FornitoreID", nullable=false)
    private TFornitoriDAO fornitore;
}
