package com.itskennedy.projectwork.dao;

import lombok.Data;

import java.util.List;

@Data
public class AcquistoDAO {
    public List<TAcquistiitemsDAO> acquisto;
    public TFattureDAO fattura;
}
