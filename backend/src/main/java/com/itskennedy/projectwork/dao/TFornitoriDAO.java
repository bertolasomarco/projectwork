package com.itskennedy.projectwork.dao;

import lombok.Data;

import javax.persistence.*;

@Entity
@Table(name = "tfornitori")
@Data
public class TFornitoriDAO {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long fornitoriID;

    @Column(name = "nome")
    String nome;
}
