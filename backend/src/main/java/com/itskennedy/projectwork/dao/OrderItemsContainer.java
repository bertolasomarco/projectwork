package com.itskennedy.projectwork.dao;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class OrderItemsContainer {
    @JsonProperty(value = "OrderItems")
    TOrderitemDAO[] items;
}
