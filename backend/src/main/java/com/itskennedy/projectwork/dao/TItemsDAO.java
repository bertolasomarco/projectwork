package com.itskennedy.projectwork.dao;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@NoArgsConstructor
@Entity
@Table(name="titems")
public class TItemsDAO
{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Long ItemID;
    @Column(name = "ASIN")
    public String asin;
    @Column(name = "Title")
    public String title;
    @Column(name = "Categoria")
    public String categoria;
    @Column(name = "Prezzo")
    public float prezzo;
    @Column(name = "Giacenza")
    public int giacenza;
    @Column(name = "Brand")
    public String brand;
    @Column(name = "SogliaDisponibilita")
    public int sogliadisponibilita;
}