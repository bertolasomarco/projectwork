package com.itskennedy.projectwork.utilis;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.itskennedy.projectwork.dao.*;
import com.itskennedy.projectwork.service.EndpointRequestMaker;
import com.itskennedy.projectwork.service.TItemsService;
import com.itskennedy.projectwork.service.TOrderitemService;
import com.itskennedy.projectwork.service.TOrdersService;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Date;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
public class ScheduledTasks {

  private static final Logger log =
      LoggerFactory.getLogger(ScheduledTasks.class);

  private static final SimpleDateFormat dateFormat =
      new SimpleDateFormat("HH:mm:ss");

  @Autowired EndpointRequestMaker endpointRequestMaker;
  @Autowired TOrdersService service;
  @Autowired TOrderitemService serviceOrderitem;
  @Autowired TItemsService serviceItems;

  @Scheduled(fixedRate = 86400001)
  public void fetchNewOrders() {
    log.info("Fetched the databases for new orders",
             dateFormat.format(new Date()));
    try {
      OrdersCointainer respone = endpointRequestMaker.getOrders(
          Instant.now().minus(500, ChronoUnit.DAYS),
          Instant.now().plus(1, ChronoUnit.DAYS));
      for (TOrdersDAO order : respone.getOrders()) {
        service.addNewOrder(order);
        OrderItemsContainer orderItems =
            endpointRequestMaker.getOrderItems(order.getAmazonOrderId());
        for (TOrderitemDAO orderitemDAO : orderItems.getItems()) {
          serviceOrderitem.addNewOrder(orderitemDAO);
          TItemsDAO item = serviceItems.getByAsin(orderitemDAO.getAsin());
          item.setGiacenza(item.getGiacenza() - orderitemDAO.getQt());
          serviceItems.updateItem(item);
          System.out.println(
              "Added new order item, ID: " + orderitemDAO.getAsin() + " x " +
              orderitemDAO.getQt() +
              " items on stock now: " + item.getGiacenza());
        }
      }
    } catch (JsonProcessingException e) {
      e.printStackTrace();
    }
  }
}
