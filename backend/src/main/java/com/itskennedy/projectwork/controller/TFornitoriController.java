package com.itskennedy.projectwork.controller;

import com.itskennedy.projectwork.dao.TFattureDAO;
import com.itskennedy.projectwork.dao.TFornitoriDAO;
import com.itskennedy.projectwork.service.TFattureService;
import com.itskennedy.projectwork.service.TFornitoriService;
import com.turkraft.springfilter.boot.Filter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("fornitori")
public class TFornitoriController {

    @Autowired
    public TFornitoriService fornitoriService;
    @GetMapping(produces = "application/json")
    @ResponseBody
    public Page<TFornitoriDAO> getAll(
            @Filter Specification<TFornitoriDAO> specification,
            Pageable pageable
    ) {
        return fornitoriService.getAll(specification, pageable);
    }
}
