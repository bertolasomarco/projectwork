package com.itskennedy.projectwork.controller;


import static org.springframework.http.HttpStatus.UNAUTHORIZED;

import com.itskennedy.projectwork.dao.LoginDAO;
import com.itskennedy.projectwork.service.JWTAuthenticationService;

import net.minidev.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;


@RestController
public class PublicEndpointsController {
    @Autowired
    private JWTAuthenticationService authenticationService;

    @PostMapping("/login")
    public ResponseEntity login(@RequestBody LoginDAO user) {
        try {
            return ResponseEntity.status(HttpStatus.OK).contentType(MediaType.APPLICATION_JSON).body(
                    authenticationService.login(user.getUsername(), user.getPassword()));
        } catch (BadCredentialsException e) {
            return ResponseEntity.status(HttpStatus.OK).contentType(MediaType.APPLICATION_JSON).body(new JSONObject().appendField("error", e.getMessage()));
        }
    }

}
