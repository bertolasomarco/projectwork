package com.itskennedy.projectwork.controller;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.itskennedy.projectwork.dao.TItemsDAO;
import com.itskennedy.projectwork.dao.TOrderitemDAO;
import com.itskennedy.projectwork.dao.TOrdersDAO;
import com.itskennedy.projectwork.service.TItemsService;
import com.itskennedy.projectwork.service.TOrderitemService;
import com.itskennedy.projectwork.service.TOrdersService;
import com.turkraft.springfilter.boot.Filter;
import net.minidev.json.JSONObject;
import net.minidev.json.parser.JSONParser;
import net.minidev.json.parser.ParseException;
import netscape.javascript.JSObject;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("orders")
public class TOrdersController {
    @Autowired
    TOrdersService service;

    @Autowired
    TItemsService itemsService;

    @Autowired
    TOrderitemService orderitemService;

    ModelMapper modelMapper = new ModelMapper();

    @GetMapping(produces = "application/json")
    @ResponseBody
    public Page<TOrdersDAO> getAllOrders(
            @Filter Specification<TOrdersDAO> specification,
            Pageable pageable
            ) {
        return service.getAll(specification, pageable);
    }

    @PostMapping(consumes = "application/json", produces = "application/json")
    public TOrdersDAO insertNewOrder(@RequestBody TOrdersDAO newOrder) {
        return this.service.addNewOrder(modelMapper.map(newOrder, TOrdersDAO.class));
    }

    @GetMapping(value = "/items/{orderId}", produces = "application/json")
    @ResponseBody
    public List<JSONObject> getItems(@PathVariable String orderId) {
        List<TOrderitemDAO> temp = orderitemService.getByOrderId(orderId);
        List<JSONObject> result = new ArrayList<>();
        for(TOrderitemDAO oi : temp){
            Gson gson = new GsonBuilder().create();
            String json = gson.toJson(itemsService.getByAsin(oi.getAsin()));// obj is your object
            JSONObject obj = null;
            try {
                obj = (JSONObject) new JSONParser(JSONParser.MODE_PERMISSIVE).parse(json);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            obj.appendField("quantity", oi.getQt());
            result.add(obj);
        }
        return result;
    }
}
