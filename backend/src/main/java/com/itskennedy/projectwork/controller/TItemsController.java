package com.itskennedy.projectwork.controller;

import java.text.SimpleDateFormat;
import java.util.*;

import com.itskennedy.projectwork.dao.TItemsDAO;
import com.itskennedy.projectwork.dao.TOrderitemDAO;
import com.itskennedy.projectwork.dao.TOrdersDAO;
import com.itskennedy.projectwork.service.TItemsService;
import com.itskennedy.projectwork.service.TOrderitemService;

import com.itskennedy.projectwork.service.TOrdersService;
import com.turkraft.springfilter.boot.Filter;
import org.apache.commons.lang3.time.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import net.minidev.json.JSONArray;
import net.minidev.json.JSONObject;

@Controller
@RequestMapping("items")
public class TItemsController {
    @Autowired
    TItemsService service;

    @Autowired
    TOrdersService orderService;

    @Autowired
    TOrderitemService orderitemService;

    @GetMapping(produces = "application/json")
    @ResponseBody
    public Page<TItemsDAO> getAllItems(
            @Filter Specification<TItemsDAO> specification,
            Pageable pageable
    ) {
        return service.getAll(specification, pageable);
    }


    @GetMapping(value = "lowItems", produces = "application/json")
    @ResponseBody
    public List<TItemsDAO> getItemsARischio() {
        return service.getItemsARischio();
    }

    @GetMapping(value = "{asin}", produces = "application/json")
    @ResponseBody
    public TItemsDAO getItemByASIN(@PathVariable String asin) {
        return service.getByAsin(asin);
    }

    @GetMapping(value = "/orders/{asin}", produces = "application/json")
    @ResponseBody
    public List<TOrderitemDAO> getItemsOrder(@PathVariable String asin ) {
        return orderitemService.getByAsin(asin);
    }

    @GetMapping(value = "/getSoldAmount", produces = "application/json")
    @ResponseBody
    public JSONArray getSoldAmount() {
        JSONArray result = new JSONArray();
        service.getAll().forEach(tItemsDAO -> {
            JSONObject json = new JSONObject();
            List<TOrderitemDAO> temp = orderitemService.getByAsin(tItemsDAO.getAsin());
            int total = 0;
            for(TOrderitemDAO dao : temp){
                total+=dao.qt;
            }
            json.appendField("title", tItemsDAO.getTitle());
            json.appendField("sold", total);
            result.add(json);
        });
        return result;
    }

    @GetMapping(value = "/getArticlesTrend", produces = "application/json")
    @ResponseBody
    public JSONArray getArticlesTrend(){
        JSONArray result = new JSONArray();
        service.getAll().forEach(tItemsDAO -> {
            JSONObject json = new JSONObject();
            json.appendField("title", tItemsDAO.getTitle());
            json.appendField("giacenza", tItemsDAO.getGiacenza());
            result.add(json);
        });
        return result;
    }

    @PostMapping(consumes = "application/json", produces = "application/json")
    @ResponseBody
    public TItemsDAO insertNewItem(@RequestBody TItemsDAO newItem) {
        return this.service.addNewItem(newItem);
    }

    @PostMapping(value = "/updateItem", consumes = "application/json", produces = "application/json")
    @ResponseBody
    public ResponseEntity updateItem(@RequestBody TItemsDAO editedItem) {
        this.service.updateItem(editedItem);
        return ResponseEntity.ok().build();
    }

    @GetMapping(value = "/delete/{id}", produces = "application/json")
    @ResponseBody
    public ResponseEntity deleteItem(@PathVariable Long id) {
        service.deleteItem(id);
        return ResponseEntity.ok().build();
    }


    @GetMapping(value = "/analisiTotale/", produces = "application/json")
    @ResponseBody
    public JSONArray analisiTotale(){
        JSONArray result = new JSONArray();
        Date startDate = new Date(0);

        int  totQt = 0;
        float totRicavi = 0;
        String weekStr = "";
        boolean count = false;

        Iterable<TOrdersDAO> tOrdersDAOIterable = orderService.getAll();
        for (Iterator<TOrdersDAO> it = tOrdersDAOIterable.iterator(); it.hasNext(); ) {
            TOrdersDAO oi = it.next();

            List<TOrderitemDAO> orderitemDAOList = orderitemService.getByOrderId(oi.amazonOrderId);
            if(DateUtils.addDays(startDate, 7).compareTo(oi.getPurchaseDate()) < 0){
                if(count){
                    JSONObject r = new JSONObject();
                    r.appendField("week", weekStr);
                    r.appendField("quantity", totQt);
                    r.appendField("revenues", totRicavi);
                    result.add(r);
                }
                totQt = 0;
                totRicavi = 0;
                startDate.setTime(oi.getPurchaseDate().getTime());
                SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
                weekStr = formatter.format(startDate)+" "+formatter.format(DateUtils.addDays(startDate,7));

                for(TOrderitemDAO or : orderitemDAOList){
                    TItemsDAO i = service.getByAsin(or.getAsin());
                    totQt += or.getQt();
                    totRicavi += or.getQt() * i.getPrezzo();

                }
                count = true;
            }else{
                for(TOrderitemDAO or : orderitemDAOList){
                    TItemsDAO i = service.getByAsin(or.getAsin());
                    totQt += or.getQt();
                    totRicavi += or.getQt() * i.getPrezzo();
                }
            }

        }
        return result;
    }

    @GetMapping(value = "/analisi/{asin}", produces = "application/json")
    @ResponseBody
    public JSONArray analisiPerItem(@PathVariable String asin){
        JSONArray result = new JSONArray();
        Date startDate = new Date(0);

        int  totQt = 0;
        float totRicavi = 0;
        String weekStr = "";
        boolean count = false;

        Iterable<TOrdersDAO> tOrdersDAOIterable = orderService.getAll();
        for (Iterator<TOrdersDAO> it = tOrdersDAOIterable.iterator(); it.hasNext(); ) {
            TOrdersDAO oi = it.next();

            List<TOrderitemDAO> orderitemDAOList = orderitemService.getByOrderIdAndAsin(oi.amazonOrderId, asin);
            if(DateUtils.addDays(startDate, 7).compareTo(oi.getPurchaseDate()) < 0){
                if(count){
                    JSONObject r = new JSONObject();
                    r.appendField("week", weekStr);
                    r.appendField("quantity", totQt);
                    r.appendField("revenues", totRicavi);
                    result.add(r);
                }
                totQt = 0;
                totRicavi = 0;
                startDate.setTime(oi.getPurchaseDate().getTime());
                SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
                weekStr = formatter.format(startDate)+" "+formatter.format(DateUtils.addDays(startDate,7));

                for(TOrderitemDAO or : orderitemDAOList){
                    TItemsDAO i = service.getByAsin(or.getAsin());
                    totQt += or.getQt();
                    totRicavi += or.getQt() * i.getPrezzo();

                }
                count = true;
            }else{
                for(TOrderitemDAO or : orderitemDAOList){
                    TItemsDAO i = service.getByAsin(or.getAsin());
                    totQt += or.getQt();
                    totRicavi += or.getQt() * i.getPrezzo();
                }
            }

        }
        return result;
    }

    @GetMapping(value = "/analisi/categoria/{categoria}", produces = "application/json")
    @ResponseBody
    public JSONArray analisiPerCategoria(@PathVariable String categoria){
        JSONArray result = new JSONArray();
        Date startDate = new Date(0);

        int  totQt = 0;
        float totRicavi = 0;
        String weekStr = "";
        boolean count = false;

        Iterable<TOrdersDAO> tOrdersDAOIterable = orderService.getAll();
        for (Iterator<TOrdersDAO> it = tOrdersDAOIterable.iterator(); it.hasNext(); ) {
            TOrdersDAO oi = it.next();

            List<TOrderitemDAO> orderitemDAOList = orderitemService.getByOrderIdAndCategoria(oi.amazonOrderId, categoria);
            if(DateUtils.addDays(startDate, 7).compareTo(oi.getPurchaseDate()) < 0){
                if(count){
                    JSONObject r = new JSONObject();
                    r.appendField("week", weekStr);
                    r.appendField("quantity", totQt);
                    r.appendField("revenues", totRicavi);
                    result.add(r);
                }
                totQt = 0;
                totRicavi = 0;
                startDate.setTime(oi.getPurchaseDate().getTime());
                SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
                weekStr = formatter.format(startDate)+" "+formatter.format(DateUtils.addDays(startDate,7));

                for(TOrderitemDAO or : orderitemDAOList){
                    TItemsDAO i = service.getByAsin(or.getAsin());
                    totQt += or.getQt();
                    totRicavi += or.getQt() * i.getPrezzo();

                }
                count = true;
            }else{
                for(TOrderitemDAO or : orderitemDAOList){
                    TItemsDAO i = service.getByAsin(or.getAsin());
                    totQt += or.getQt();
                    totRicavi += or.getQt() * i.getPrezzo();
                }
            }

        }
        return result;
    }
}

