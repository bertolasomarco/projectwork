package com.itskennedy.projectwork.controller;

import com.itskennedy.projectwork.dao.*;
import com.itskennedy.projectwork.service.*;
import com.turkraft.springfilter.boot.Filter;
import net.minidev.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@Controller
@RequestMapping("acquisti")
public class AcquistiController {

    @Autowired
    public TFattureService fattureService;
    @Autowired
    public TAcquistiitemsService acquistoService;
    @Autowired
    public TItemsService itemsService;

    @GetMapping(produces = "application/json")
    @ResponseBody
    public Page<TFattureDAO> getAllItems(
            @Filter Specification<TFattureDAO> specification,
            Pageable pageable
    ) {
        return fattureService.getAll(specification, pageable);
    }

    @PostMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity insertNewAcquisto(@RequestBody AcquistoDAO acquistoDAO) {
       Long acquistoID = fattureService.addNewFattura(acquistoDAO.getFattura()).getAcquistoID();
       for(TAcquistiitemsDAO aq : acquistoDAO.getAcquisto()) {
           aq.setAcquistoID(acquistoID);
           acquistoService.addNewAcquisto(aq);
           Optional<TItemsDAO> opt = itemsService.getById(aq.getItems().getItemID());
           if (opt.isPresent()) {
               TItemsDAO item = opt.get();
               item.setGiacenza(item.getGiacenza() + aq.getQauantitaAcquistata());
               itemsService.updateItem(item);
               System.out.println(
                       "Added new order item, ID: " + item.getAsin() + " x " +
                               aq.getQauantitaAcquistata() +
                               " items on stock now: " + item.getGiacenza());
           }
       }
       return ResponseEntity.ok().build();
    }

    @PostMapping(value = "firmaFattura/{acquistoId}", consumes = "application/json", produces =  "application/json")
    public ResponseEntity firmaFattura(@PathVariable long acquistoId){
       Optional<TFattureDAO> opt = fattureService.getByAcquistoId(acquistoId);
       if(opt.isPresent()){
           TFattureDAO fattura = opt.get();
           fattura.setEffettuata(true);
           fattureService.updateFattura(fattura);
           return ResponseEntity.ok().build();
       }else {
           return ResponseEntity.notFound().build();
       }
    }

    @GetMapping(value = "totaleSpesa/{acquistoId}", produces = "application/json")
    @ResponseBody
    public JSONObject totaleSpesa(@PathVariable long acquistoId){
       JSONObject obj = new JSONObject();
       obj.appendField("acquistoId", acquistoId);
       obj.appendField("totale", acquistoService.getSpesaTotale(acquistoId));
       return obj;
    }

    @GetMapping(value = "items/{acquistoId}", produces = "application/json")
    @ResponseBody
    public List<TAcquistiitemsDAO> acquistiItem(@PathVariable long acquistoId){
        return acquistoService.getAcquistoItems(acquistoId);
    }

    @PostMapping(value = "update",consumes = "application/json", produces = "application/json")
    public void upadateAcquisto(@RequestBody AcquistoDAO acquistoDAO) {
        deleteAcquisto(acquistoDAO.getFattura().getAcquistoID());
        fattureService.updateFattura(acquistoDAO.getFattura());
        Long acquistoID = acquistoDAO.getFattura().getAcquistoID();
        for(TAcquistiitemsDAO aq : acquistoDAO.getAcquisto()){
            aq.setAcquistoID(acquistoID);
            acquistoService.addNewAcquisto(aq);
            Optional<TItemsDAO> opt = itemsService.getById(aq.getItems().getItemID());
            if(opt.isPresent()){
                TItemsDAO item = opt.get();
                item.setGiacenza(item.getGiacenza() + aq.getQauantitaAcquistata());
                itemsService.updateItem(item);
                System.out.println(
                        "Added new order item, ID: " + item.getAsin() + " x " +
                                aq.getQauantitaAcquistata() +
                                " items on stock now: " + item.getGiacenza());
            }
        }
    }


    @GetMapping(value = "delete/{acquistoId}", produces = "application/json")
    @ResponseBody
    public ResponseEntity deleteItem(@PathVariable long acquistoId){
        deleteAcquisto(acquistoId);
        fattureService.deleteFatturaById(acquistoId);
        return ResponseEntity.ok().build();
    }

    private void deleteAcquisto(long acquistoID) {
        List<TAcquistiitemsDAO> temp = acquistoService.getAcquistoItems(acquistoID);
        for(TAcquistiitemsDAO aq : temp){
            Optional<TItemsDAO> opt = itemsService.getById(aq.getItems().getItemID());
            if(opt.isPresent()){
                TItemsDAO item = opt.get();
                item.setGiacenza(item.getGiacenza() - aq.getQauantitaAcquistata());
                itemsService.updateItem(item);
                System.out.println(
                        "Removed new order item, ID: " + item.getAsin() + " x " +
                                aq.getQauantitaAcquistata() +
                                " items on stock now: " + item.getGiacenza());
            }
        }
        acquistoService.deleteAllByAcquisto(acquistoID);
    }
}

