package com.itskennedy.projectwork.service;

import com.itskennedy.projectwork.dao.TOrderitemDAO;

import java.util.List;

public interface TOrderitemService {
    Iterable<TOrderitemDAO> getAll();
    void addNewOrder(TOrderitemDAO orderItem);
    List<TOrderitemDAO> getByOrderId(String orderid);
    List<TOrderitemDAO> getByAsin(String asin);

    List<TOrderitemDAO> getByOrderIdAndAsin(String amazonOrderId, String itemId);

    List<TOrderitemDAO> getByOrderIdAndCategoria(String amazonOrderId, String categoria);
}
