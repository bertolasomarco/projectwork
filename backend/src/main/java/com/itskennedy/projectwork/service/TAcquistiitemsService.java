package com.itskennedy.projectwork.service;

import com.itskennedy.projectwork.dao.TAcquistiitemsDAO;
import com.itskennedy.projectwork.dao.TFattureDAO;

import java.util.List;

public interface TAcquistiitemsService {
    TAcquistiitemsDAO addNewAcquisto(TAcquistiitemsDAO fattura);
    float getSpesaTotale(Long aquistoId);
    void deleteAllByAcquisto(Long acquistoId);

    List<TAcquistiitemsDAO> getAcquistoItems(long acquistoId);
}
