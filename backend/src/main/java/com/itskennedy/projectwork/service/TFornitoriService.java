package com.itskennedy.projectwork.service;

import com.itskennedy.projectwork.dao.TFornitoriDAO;
import com.itskennedy.projectwork.dao.TItemsDAO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;

public interface TFornitoriService {
    TFornitoriDAO getByFornitoriID(Long fornitoriID);
    Iterable<TFornitoriDAO> getAll();
    Page<TFornitoriDAO> getAll(Specification<TFornitoriDAO> specification, Pageable pageable);
    void addNewFornitori(TFornitoriDAO fornitori);
    void updateFornitori(TFornitoriDAO oldFornitori);
    void deleteFornitori(long id);
}
