package com.itskennedy.projectwork.service;


import com.itskennedy.projectwork.dao.TAcquistiitemsDAO;
import com.itskennedy.projectwork.dao.TFattureDAO;
import com.itskennedy.projectwork.repository.TAcquistiitemsRepository;
import com.itskennedy.projectwork.repository.TFattureRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
public class TAcquistiitemsServiceImpl implements TAcquistiitemsService{

    @Autowired
    TAcquistiitemsRepository repo;

    @Override
    public TAcquistiitemsDAO addNewAcquisto(TAcquistiitemsDAO fattura) {
        return repo.save(fattura);
    }

    @Override
    public float getSpesaTotale(Long acquistoId) {
       List<TAcquistiitemsDAO> temp = repo.getAllByAcquistoID(acquistoId);
       float res = 0;
       for(TAcquistiitemsDAO item : temp){
          res += item.getQauantitaAcquistata() * item.getPrezzoUnitarioAcquisto();
       }
       return res;
    }

    @Override
    public void deleteAllByAcquisto(Long acquistoId) {
        repo.deleteAllByAcquistoID(acquistoId);
    }

    @Override
    public List<TAcquistiitemsDAO> getAcquistoItems(long acquistoId) {
        return repo.getAllByAcquistoID(acquistoId);
    }
}
