package com.itskennedy.projectwork.service;

import com.itskennedy.projectwork.dao.TItemsDAO;
import com.itskennedy.projectwork.repository.TItemRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class TItemsServiceImpl implements TItemsService {

    @Autowired
    private TItemRepository repo;

    @Override
    public List<TItemsDAO> getItemsARischio() {
        return repo.getItemsARischio();
    }

    @Override
    public TItemsDAO getByAsin(String ASIN) {
        return repo.getTItemsDAOByAsin(ASIN);
    }

    @Override
    public Page<TItemsDAO> getAll(Specification<TItemsDAO> specification, Pageable pageable) {
        return repo.findAll(specification, pageable);
    }

    @Override
    public Iterable<TItemsDAO> getAll() {
       return repo.findAll();
    }

    @Override
    public Optional<TItemsDAO> getById(Long itemID) {
        return repo.findById(itemID);
    }

    @Override
    public TItemsDAO addNewItem(TItemsDAO item) {
        item.setItemID(null);
        return repo.save(item);
    }

    @Override
    public void updateItem(TItemsDAO oldItem) {
        repo.saveAndFlush(oldItem);
    }

    @Override
    public void deleteItem(long id) {
        TItemsDAO temp = new TItemsDAO();
        temp.setItemID(id);
        repo.delete(temp);
    }


}
