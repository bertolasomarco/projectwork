package com.itskennedy.projectwork.service;

import com.itskennedy.projectwork.dao.TUserDAO;
import com.itskennedy.projectwork.repository.TUserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class TUserService {
    @Autowired
    private TUserRepository userRepository;

    public TUserDAO save(TUserDAO user) {
        return userRepository.save(user);
    }

    public Optional<TUserDAO> getByToken(String token) {
        return userRepository.findByToken(token);
    }

    public Optional<TUserDAO> getByUsername(String username) {
        return userRepository.findByUsername(username);
    }
}
