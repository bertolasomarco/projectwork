package com.itskennedy.projectwork.service;

import com.itskennedy.projectwork.dao.TOrdersDAO;
import com.itskennedy.projectwork.repository.TOrdersRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Optional;

@Service
@Transactional
public class TOrdersServiceImpl implements TOrdersService {

    @Autowired
    private TOrdersRepository repo;

    @Override
    public Iterable<TOrdersDAO> getAll() {
        return  repo.findAll();
    }

    @Override
    public Optional<TOrdersDAO> getById(String id) {
        return repo.findById(id);
    }

    @Override
    public TOrdersDAO addNewOrder(TOrdersDAO order) {
       return repo.save(order);
    }

    @Override
    public Page<TOrdersDAO> getAll(Specification<TOrdersDAO> specification, Pageable pageable) {
        return repo.findAll(specification, pageable);
    }

}
