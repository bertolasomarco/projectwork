package com.itskennedy.projectwork.service;

import com.itskennedy.projectwork.dao.TUserDAO;
import net.minidev.json.JSONObject;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.AuthenticationException;


public interface UserAuthenticationService {
    JSONObject login(String username, String password) throws BadCredentialsException;

    TUserDAO authenticateByToken(String token) throws AuthenticationException;

    void logout(String username);

}
