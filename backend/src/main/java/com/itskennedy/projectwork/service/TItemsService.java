package com.itskennedy.projectwork.service;

import com.itskennedy.projectwork.dao.TItemsDAO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;

import java.util.List;
import java.util.Optional;

public interface TItemsService {
    List<TItemsDAO> getItemsARischio();
    TItemsDAO getByAsin(String ASIN);
    TItemsDAO addNewItem(TItemsDAO item);
    void updateItem(TItemsDAO oldItem);
    void deleteItem(long id);
    Page<TItemsDAO> getAll(Specification<TItemsDAO> specification, Pageable pageable);
    Iterable<TItemsDAO> getAll();
    Optional<TItemsDAO> getById(Long itemID);
}
