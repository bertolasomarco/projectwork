package com.itskennedy.projectwork.service;

import com.itskennedy.projectwork.dao.TOrderitemDAO;
import com.itskennedy.projectwork.repository.TOrderitemRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TOrderitemServiceImpl implements TOrderitemService {
    @Autowired
    TOrderitemRepository repo;

    @Override
    public Iterable<TOrderitemDAO> getAll() {
        return repo.findAll();
    }

    @Override
    public void addNewOrder(TOrderitemDAO orderItem) {
        repo.save(orderItem);
    }

    @Override
    public List<TOrderitemDAO> getByOrderId(String orderid) {
        return repo.getTOrderitemDAOSByAmazonOrderId(orderid);
    }

    @Override
    public List<TOrderitemDAO> getByAsin(String asin) {
        return repo.getTOrderitemDAOSByAsin(asin);
    }

    @Override
    public List<TOrderitemDAO> getByOrderIdAndAsin(String amazonOrderId, String itemId) {
        return repo.getTOrderitemDAOSByAmazonOrderIdAndAsin(amazonOrderId, itemId);
    }

    @Override
    public List<TOrderitemDAO> getByOrderIdAndCategoria(String amazonOrderId, String categoria) {
        return repo.getTOrderitemDAOSByAmazonOrderIdAndCategoria(amazonOrderId, categoria);
    }
}
