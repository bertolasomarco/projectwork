package com.itskennedy.projectwork.service;

import java.time.Duration;
import java.time.Instant;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.itskennedy.projectwork.dao.OrderItemsContainer;
import com.itskennedy.projectwork.dao.OrdersCointainer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;

@Service
public class EndpointRequestMaker {
    private static final Duration REQUEST_TIMEOUT = Duration.ofSeconds(30);

    private final WebClient localApiClient;

    @Autowired
    public EndpointRequestMaker(WebClient localApiClient) {
        this.localApiClient = localApiClient;
    }

    public OrdersCointainer getOrders(Instant createdAfter, Instant createdBefore) throws JsonProcessingException {
        String url = "/orders?refresh_token=Atzr|IwEBIPGGbogA4gJ86OciHsp16r6gXmV&CreatedAfter="+createdAfter.toString()+"&CreatedBefore=2021-07-31T16:09:52.000";
        String json =  localApiClient
                .get()
                .uri(url)
                .retrieve()
                .bodyToMono(String.class)
                .block(REQUEST_TIMEOUT);
        return new ObjectMapper().readValue(json, OrdersCointainer.class);
    }

    public OrderItemsContainer getOrderItems(String orderId) throws JsonProcessingException {
        String url = "/orderitems?refresh_token=Atzr|IwEBIPGGbogA4gJ86OciHsp16r6gXmV&AmazonOrderId=" + orderId;
        String json =  localApiClient
                .get()
                .uri(url)
                .retrieve()
                .bodyToMono(String.class)
                .block(REQUEST_TIMEOUT);
        return new ObjectMapper().readValue(json, OrderItemsContainer.class);
    }

}
