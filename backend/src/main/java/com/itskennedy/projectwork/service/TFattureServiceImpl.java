package com.itskennedy.projectwork.service;


import com.itskennedy.projectwork.dao.TFattureDAO;
import com.itskennedy.projectwork.dao.TItemsDAO;
import com.itskennedy.projectwork.repository.TFattureRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Optional;

@Service
@Transactional
public class TFattureServiceImpl implements TFattureService{

    @Autowired
    private TFattureRepository repo;

    @Override
    public TFattureDAO getByNumeroFattura(String NumeroFattura) {
        return repo.getFirstByNumeroFattura(NumeroFattura);
    }

    @Override
    public Iterable<TFattureDAO> getAll() { return repo.findAll();}

    @Override
    public TFattureDAO addNewFattura(TFattureDAO fattura){
        fattura.setNumeroFattura(null);
        fattura.setEffettuata(false);
        return repo.save(fattura);
    }

    @Override
    public void updateFattura(TFattureDAO oldFattura) {
        repo.saveAndFlush(oldFattura);
    }

    @Override
    public void deleteFattura(String id){
        repo.deleteByNumeroFattura(id);
    }

    @Override
    public Optional<TFattureDAO> getByAcquistoId(long acquistoId) {
        return repo.findById(acquistoId);
    }

    @Override
    public Page<TFattureDAO> getAll(Specification<TFattureDAO> specification, Pageable pageable) {
        return repo.findAll(
                specification,
                pageable
        );
    }

    @Override
    public void deleteFatturaById(long acquistoId) {
        repo.deleteById(acquistoId);
    }
}
