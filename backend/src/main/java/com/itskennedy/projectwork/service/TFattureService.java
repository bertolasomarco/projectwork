package com.itskennedy.projectwork.service;

import com.itskennedy.projectwork.dao.TFattureDAO;
import com.itskennedy.projectwork.dao.TItemsDAO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;

import java.util.Optional;

public interface TFattureService {
    TFattureDAO getByNumeroFattura(String NumeroFattura);
    Iterable<TFattureDAO> getAll();
    TFattureDAO addNewFattura(TFattureDAO fattura);
    void updateFattura(TFattureDAO oldFattura);
    void deleteFattura(String id);
    Optional<TFattureDAO> getByAcquistoId(long acquistoId);

    Page<TFattureDAO> getAll(Specification<TFattureDAO> specification, Pageable pageable);

    void deleteFatturaById(long acquistoId);
}
