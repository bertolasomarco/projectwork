package com.itskennedy.projectwork.service;

import com.itskennedy.projectwork.dao.TOrdersDAO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;

import java.util.Optional;

public interface TOrdersService {
    Iterable<TOrdersDAO> getAll();
    Optional<TOrdersDAO> getById(String id);
    TOrdersDAO addNewOrder(TOrdersDAO order);

    Page<TOrdersDAO> getAll(Specification<TOrdersDAO> specification, Pageable pageable);
}
