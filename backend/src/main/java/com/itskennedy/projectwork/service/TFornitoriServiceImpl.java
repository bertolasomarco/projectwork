package com.itskennedy.projectwork.service;

import com.itskennedy.projectwork.dao.TFornitoriDAO;
import com.itskennedy.projectwork.repository.TFornitoriRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
@Transactional
public class TFornitoriServiceImpl implements TFornitoriService{

    @Autowired
    private TFornitoriRepository repo;

    @Override
    public TFornitoriDAO getByFornitoriID(Long fornitoriID) {
        return repo.getFirstByFornitoriID(fornitoriID);
    }

    @Override
    public Iterable<TFornitoriDAO> getAll() { return repo.findAll();}

    @Override
    public Page<TFornitoriDAO> getAll(Specification<TFornitoriDAO> specification, Pageable pageable) {
        return repo.findAll(specification, pageable);
    }

    @Override
    public void addNewFornitori(TFornitoriDAO fornitori) {
        fornitori.setFornitoriID(null);
        repo.save(fornitori);
    }

    @Override
    public void updateFornitori(TFornitoriDAO oldFornitori) {
        repo.save(oldFornitori);
    }

    @Override
    public void deleteFornitori(long id) {
        TFornitoriDAO temp = new TFornitoriDAO();
        temp.setFornitoriID(id);
        repo.delete(temp);
    }


}
