INSERT INTO titems ( ASIN, Title, Categoria, Prezzo, Giacenza, Brand, SogliaDisponibilita) VALUES ( N'B08CJHC9MT', N'Maglia Nike Inter 2020-2021', N'Maglia Calcio', 56, 1012, N'Nike', 25),
    ( N'B07GJ19DGP', N'PUMA AC Milan Home Shirt Replica SS with Sponsor Logo, Maglietta Uomo, Rosso (Tango Red Black), L', N'Maglie Calcio', 45, 1109, N'Puma', 25),
    ( N'B07VJL4KWQ', N'adidas Juve H JSY, T-Shirt Uomo, White/Black, L', N'Maglie Calcio', 44, 999, N'Adidas', 30),
    ( N'B07D9SB7XW', N'Minecraft - Nintendo Switch', N'Videogiochi', 21.3, 999, N'Nintendo', 200),
    ( N'B07VK4QKBP', N'homcom Set Bilanciere e Dischi 70kg, 8 Dischi Pesi Rivestiti in PVC e Barra in Ferro, 170x40.5x40.5cm Nero', N'Palestra', 163.89, 999, N'Pull', 11),
    ( N'B08KSS6CLT', N'Lipton Tè Nero alla Pesca, 20 Pezzi, 700g', N'Bibite', 1.68, 100, N'Lipton', 10),
    ( N'B08123PCJH', N'Aria Fritta ANTISFIGA Spray BOMBOLETTA Alluminio 100 ML', N'Giochi', 9.02, 100, N'Spra', 500),
    ( N'B07K495TYN', N'Navaris Luce notturna LED Unicorno - Lampada per bambini ricaricabile - Lucina da notte per cameretta con cambio colori e cavo Micro USB - bianca', N'Illuminazione', 18.99, 999, N'Navaris', 22);

INSERT INTO TFornitori ( Nome) VALUES ( N'Bert Industrie'),
    ( N'Fik Company'),
    ( N'LukTwe Spa');

INSERT INTO tusers ( Username, Password) values ('admin','root'),
    ('utente1','root');
