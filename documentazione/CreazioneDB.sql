CREATE DATABASE projectwork2021;
use projectwork2021;
CREATE TABLE TOrders(
    AmazonOrderId VARCHAR(500) PRIMARY KEY,
    PurchaseDate DATETIME ,
    LastUpdateDate DATETIME ,
    OrderStatus VARCHAR(500),
    FulfillmentChannel VARCHAR(500),
    NumberOfItemsShipped INT ,
    NumberOfItemsUnshipped INT ,
    PaymentMethod VARCHAR(500),
    PaymentMethodDetails VARCHAR(500),
    MarketplaceId VARCHAR(500),
    ShipmentServiceLevelCategory VARCHAR(500),
    OrderType VARCHAR(500),
    EarliestShipDate DATETIME ,
    LatestShipDate DATETIME ,
    IsBusinessOrder BIT ,
    IsPrime BIT ,
    IsGlobalExpressEnabled BIT ,
    IsPremiumOrder BIT ,
    IsSoldByAB BIT ,
    CompanyLegalName VARCHAR(500),
    BuyerEmail VARCHAR(500),
    BuyerName VARCHAR(500),
    PurchaseOrderNumber VARCHAR(500),
    ShippingAddressName VARCHAR(500),
    ShippingAddressLine1 VARCHAR(500),
    ShippingAddressCity VARCHAR(500),
    ShippingCityStateOrRegion VARCHAR(500),
    ShippingStateOrRegionPostalCode VARCHAR(500)
);

CREATE TABLE TFatture(
    AcquistoID INT  AUTO_INCREMENT primary key,
	FornitoreID INT,
	DataFattura DATETIME,
	NumeroFattura VARCHAR(500),
	Effettuata BIT
);

CREATE TABLE TItems(
	ItemID INT  AUTO_INCREMENT primary key,
	ASIN VARCHAR(500),
	Title VARCHAR(500),
	Categoria VARCHAR(500),
	Prezzo FLOAT,
	Giacenza int,
	Brand VARCHAR(500),
    	SogliaDisponibilita int
);

CREATE TABLE TFornitori(
	FornitoriID INT AUTO_INCREMENT primary key,
	Nome VARCHAR(500)
);

CREATE TABLE TUsers(
	UsersID INT  AUTO_INCREMENT primary key,
	Username VARCHAR(500),
    Password VARCHAR(500),
    Token VARCHAR(500)
);

CREATE TABLE TAcquistiItems
(
    AcquistoItemID         INT AUTO_INCREMENT primary key,
    AcquistoID             INT,
    ItemID                 INT,
    QuantitaAcquistata     INT,
    PrezzoUnitarioAcquisto FLOAT
);


CREATE TABLE TOrderItem
(
    OrderItemID    INT primary key,
    amazonOrderId  VARCHAR(50),
    ASIN           VARCHAR(50),
    qt             INT
)
