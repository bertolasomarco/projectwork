import { Injectable } from '@angular/core';
import { ApiService } from './api.service';
import { HttpClient } from '@angular/common/http';
@Injectable({
  providedIn: 'root'
})
export class GraficiService {

  public path: string = "items";

  constructor(public api: ApiService, public http: HttpClient) { }

  public getGraficoaBarreSett(){
    return this.api.get(this.path + "/analisiTotale/", null);
  }

  public getGraficoaTorta(){
    return this.api.get(this.path + "/getSoldAmount/", null);
  }

  public getGraficoaBarre(){
    return this.api.get(this.path, null);
  }

  public getQtaSoldItemsWeek(asin){
    return this.api.get(this.path + "/analisi/" + asin, null);
  }

  public getQtaSoldCategoryWeek(categoria){
    return this.api.get(this.path + "/analisi/categoria/" + categoria, null);
  }


}
