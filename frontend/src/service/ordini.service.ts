import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ApiService } from './api.service';

@Injectable({
  providedIn: 'root'
})
export class OrdiniService {
  public path: string = "orders";
  constructor(public api: ApiService, public http: HttpClient) { }

  public getOrdini(filtri:any){
    return this.api.get(this.path+"?" +filtri, null);
  }
  public getArticoliOrdine(id:any){
    return this.api.get(this.path+"/items/"+id, null);
  }

}
