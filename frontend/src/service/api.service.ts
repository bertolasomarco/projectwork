import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { throwError as observableThrowError, Observable } from "rxjs";
import { map, catchError } from 'rxjs/operators';
import { Header } from 'primeng/api';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  private readonly host = "http://localhost:8080";
  constructor(private http: HttpClient) {}
 
  private formatErrors(error: any) {
 
    if (error.status === 401) {
      console.log('Unauthorized');
      sessionStorage.removeItem('jwt');
      return observableThrowError('Unauthorized');
    } else if (error.status === 404) {
      console.log('Not Found');
      return observableThrowError('Not Found');
    } else if (error.status === 500) {
      if (error.error.status === 'error') {
        return observableThrowError(error.error.message);
      }else{
        console.log("Internal server error");
      }    
      if (error.error.code === '23505') {
        return observableThrowError('Duplicate key');
      }
      else {
        return observableThrowError('Internal Server Error');
      }
    } else if (error.status === 503) {
      console.log('Service Unavailable');
      return observableThrowError('Service Unavailable');
    } else if (error.message) {
      return observableThrowError(error.message);
    } else {
      return observableThrowError(error.json());
    }
 
  }
 
  public get(path: string, params:any): Observable<any> {
    let Headers = new HttpHeaders();
    Headers = Headers.append('Content-Type', 'application/x-www-form-urlencoded');
    Headers = Headers.append('Accept', 'application/json');
    Headers = this.setAuth(Headers);
    return this.http.get(this.host + "/" + path,
      { headers: Headers, params: params, withCredentials: false }).pipe(
        catchError(this.formatErrors),
        map((res: Response) => {
          return res;
        }));
  }
  
  public post(path: string, body: any): Observable<any> {
    let Headers = new HttpHeaders();
    Headers = Headers.append('Content-Type', 'application/json');
    Headers = Headers.append('Accept', '*/*');
    Headers = this.setAuth(Headers);

    return this.http.post(this.host + "/" + path, body,
      { headers: Headers,  withCredentials: false}).pipe(
        catchError(this.formatErrors),
        map((res: any) => {
          return res;
        }));
  }
  
  public delete(path: string, params:any): Observable<any> {
    let Headers = new HttpHeaders();
    Headers = Headers.append('Content-Type', 'application/x-www-form-urlencoded');
    Headers = Headers.append('Accept', 'application/json');
    this.setAuth(Headers);
 
    return this.http.delete(this.host + "/" + path,
      { headers: Headers, params: params, withCredentials: false }).pipe(
        catchError(this.formatErrors),
        map((res: Response) => {
          return res;
        }));
  }

  public setAuth(headers: HttpHeaders){
    if(sessionStorage.getItem("jwt") !=null){
      return headers.append("Authorization", sessionStorage.getItem("jwt"));
    }else{
      return headers;
    }
   
  }


}
