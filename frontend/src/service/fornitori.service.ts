import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ApiService } from './api.service';

@Injectable({
  providedIn: 'root'
})
export class FornitoriService {
  public path: string = "fornitori";
  constructor(public api: ApiService, public http: HttpClient) { }

  public getFornitori(filtri:any){
    return this.api.get(this.path+"?" +filtri, null);
  }
}
