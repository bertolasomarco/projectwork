import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ApiService } from './api.service';

@Injectable({
  providedIn: 'root'
})
export class AcquistiService {
  public path: string = "acquisti";
  constructor(public api: ApiService, public http: HttpClient) { }

  public getAcquisti(filtri:any){
    return this.api.get(this.path +"?"+filtri, null);
  }

  public firma(id:any){
    return this.api.post(this.path +"/firmaFattura/"+id, null);
  }

  public totSpesa(id:any){
    return this.api.get(this.path +"/totaleSpesa/"+id, null);
    
  }

  public modifica(body:any){
    return this.api.post(this.path+"/update", body);
  }
  public insert(body:any){
    return this.api.post(this.path, body);
  }

  public elimina(id:any){
    return this.api.get(this.path+"/delete/"+id, null);
  }

  public listaArticoli(id:any){
    return this.api.get(this.path+"/items/"+id, null);
  }
}
