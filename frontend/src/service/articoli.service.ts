import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ApiService } from './api.service';

@Injectable({
  providedIn: 'root'
})
export class ArticoliService {
  public path: string = "items";
  constructor(public api: ApiService, public http: HttpClient) { }

  public getArticoli(filtri:any){
    return this.api.get(this.path+"?" +filtri, null);
  }
  public Insert(body:any){
    return this.api.post(this.path, body);
  }
  public Modifica(body:any){
    return this.api.post(this.path+"/updateItem", body);
  }

  public Elimina(id:any){
    return this.api.get(this.path+"/delete/" +id, null);
  }

}
