import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthenticationGuard } from '../auth/authentication.guard';

// Import Containers
import { DefaultLayoutComponent } from './containers';
import { AcquistiModule } from './views/acquisti/acquisti.module';
import { ArticoliModule } from './views/articoli/articoli.module';
import { GraficiModule } from './views/grafici/grafici.module';
import { LoginComponent } from './views/login/login.component';
import { OrdiniModule } from './views/ordini/ordini.module';

import { P404Component } from './views/error/404.component';
import { P500Component } from './views/error/500.component';

export const routes: Routes = [
  {
    path: '',
    redirectTo: 'grafici',
    pathMatch: 'full',
  },
  {
    path: '404',
    component: P404Component,
    data: {
      title: 'Page 404'
    }
  },
  {
    path: '500',
    component: P500Component,
    data: {
      title: 'Page 500'
    }
  },
  {
    path: 'login',
    component: LoginComponent,
    data: {
      title: 'Login Page'
    }
  },
  {
    path: '',
    component: DefaultLayoutComponent,
    data: {
      title: 'Home'
    },
    children: [
      {
        path: 'grafici',
        loadChildren: () => {return GraficiModule;},
        canActivate: [AuthenticationGuard]
      },
      {
        path: 'acquisti', 
        loadChildren: () => {return AcquistiModule;},
        canActivate: [AuthenticationGuard]
      },
      {
        path: 'articoli',
        loadChildren: () => {return ArticoliModule;},
        canActivate: [AuthenticationGuard]
      },
      {
        path: 'ordini',
        loadChildren: () => {return OrdiniModule;},
        canActivate: [AuthenticationGuard]
      }
    ]
  },
  { path: '**', component: P404Component }
];

@NgModule({
  imports: [ RouterModule.forRoot(routes, { relativeLinkResolution: 'legacy' }) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule {}
