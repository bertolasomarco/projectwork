import { INavData } from '@coreui/angular';

export const navItems: INavData[] = [

  {
    name: 'Grafici',
    url: '/grafici',
    icon: 'cil-chart-line'
  },
  {
    name: 'Articoli',
    url: '/articoli',
    icon: 'cil-tags'
  },
  {
    name: 'Acquisti',
    url: '/acquisti',
    icon: 'cil-truck'
  },
  {
    name: 'Ordini',
    url: '/ordini',
    icon: 'cil-truck'
  },
];
