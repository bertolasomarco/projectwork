import { Component, OnInit } from '@angular/core';
import { LazyLoadEvent } from 'primeng/api';
import { DataTableOptions } from '../../../../api/datatable-options';
import { ArticoliService } from '../../../../service/articoli.service';

@Component({
  selector: 'app-articoli',
  templateUrl: './articoli.component.html',
  styleUrls: ['./articoli.component.scss']
})
export class ArticoliComponent implements OnInit {

  lista: any[]
  tabella: DataTableOptions;
  nominativo: any;
  index: any;
  mostraDialog: boolean;
  mostraDialog3: boolean;
  nuovo: boolean;
  infoSelect: any[];
  infoSelect2: any[] = [{ "name": "ASIN", "value": "" },
  { "name": "Title", "value": "" },
  { "name": "Categoria", "value": "" },
  { "name": "Prezzo", "value": "" },
  { "name": "Giacenza", "value": "" },
  { "name": "Brand", "value": "" },
  { "name": "Soglia Disponibilità", "value": "" }];
  datiu: any[];
  mostraDialogDelete: boolean = false;
  nomeelimina: any;
  tipodialog: string = "articolo";
  IdElimina: any;
  filtri: any = "";
  totalRecord = 0;
  page = 0;
  size = 10;

  constructor(public articoliService: ArticoliService) { }

  ngOnInit(): void {

    this.tabella = {
      colsOptions: [
        { label: "asin", name: "ASIN", type: "text" },
        { label: "title", name: "Nome", type: "text" },
        { label: "categoria", name: "Categoria", type: "text" },
        { label: "prezzo", name: "Prezzo", type: "numeric" },
        { label: "giacenza", name: "Giacenza", type: "numeric" },
        //{ label: "Brand", name: "Brand", type: "text" }
      ],
    }

  }

  nascondi() {
    this.mostraDialog = false;
  }
  nascondi2() {
    this.mostraDialog3 = false;
  }
  buttonDettClick(event) {
    this.nuovo = false
    this.mostraDialog = true;
    this.index = 1;
    this.popolaInfo(event);
  }

  buttonEliminaClick(event) {
    this.nomeelimina = event.item.title
    this.IdElimina = event.item.ItemID
    this.mostraDialogDelete = true
  }

  popolaInfo(event: any) {
    this.datiu = event.item;
    this.nominativo = "Informazioni sull'articolo: " + this.datiu['title'];
    //servizio
    this.infoSelect = [
      { name: 'ID', value: this.datiu['ItemID'] },
      { name: 'ASIN', value: this.datiu['asin'] },
      { name: 'Nome', value: this.datiu['title'] },
      { name: 'Categoria', value: this.datiu['categoria'] },
      { name: 'Prezzo', value: this.datiu['prezzo'] },
      { name: 'Giacenza', value: this.datiu['giacenza'] },
      { name: 'Brand', value: this.datiu['brand'] },
      { name: 'Soglia Disponibilità', value: this.datiu['sogliadisponibilita'] },

    ];
  }

  VediAgg(event) {
    this.nuovo = true
    this.nominativo = "Inserisci un nuovo articolo";
    this.infoSelect = [
      { name: 'ASIN', value: "" },
      { name: 'Nome', value: "" },
      { name: 'Categoria', value: "" },
      { name: 'Prezzo', value: "" },
      { name: 'Giacenza', value: "" },
      { name: 'Brand', value: "" },
      { name: 'Soglia Disponibilità', value: "" },

    ];
    this.mostraDialog = true;
  }

  EliminaArticolo() {

    console.log("Id da eliminare:", this.IdElimina)
    this.articoliService.Elimina(this.IdElimina).subscribe(res=>{
      this.aggiornaTabella()
    })
    this.mostraDialogDelete = false
  }

  LoadArticoli(event: LazyLoadEvent) {
    // this.loading = true;
    //console.log(event)
    this.articoliService.getArticoli(this.filtri+"&size="+this.size+"&page="+this.page).subscribe((res => {
      this.lista = res.content;
      this.totalRecord = res.totalElements;
    }))
  }
  Paginazione(event: any){
    console.log(event)

    this.page = event.first/event.rows
    this.size = event.rows

    this.articoliService.getArticoli(this.filtri+"&size="+this.size+"&page="+this.page).subscribe((res => {
      this.lista = res.content;
      this.totalRecord = res.totalElements;
    }))
    
  }

  InserimentoFiltri(event: any) {
    this.filtri = ""
   // console.log("filtri ", event.filters);
    Object.keys(event.filters).forEach(element => {
     // console.log(element)
      var tipo = element;
     // console.log("VEDOOO: ",event.filters[element])
      var conta = 0;
     if(event.filters[element][0].value!=null){
      this.filtri+="(";
      event.filters[element].forEach(element => {
       // console.log("ELEMENTTTTTTTTTTTT: ", element)
        if (conta == 1) {
          this.filtri += " " + element.operator + " "
        }
        if (element.value != null) {
          if(element.matchMode == "notEquals"){
            this.filtri += "not("+tipo
          }
          else{
            this.filtri += tipo
          }
          
          if (element.matchMode == "startsWith") {
            this.filtri += "~'" + element.value + "*'"
          }
          if (element.matchMode == "contains") {
            this.filtri += "~'*" + element.value + "*'"
          }
          if (element.matchMode == "notContains") {
            this.filtri += "!~'*" + element.value + "*'"
          }
          if (element.matchMode == "endWith") {
            this.filtri += "~'*" + element.value + "'"
          }
          if (element.matchMode == "equals") {
            if (typeof element.value == "number") {
              this.filtri += ":" + element.value + ""
            }
            else {
              this.filtri += "~'" + element.value + "'"
            }

          }
          if (element.matchMode == "notEquals") {
            if (typeof element.value == "number") {
              this.filtri += ":" + element.value + ")"
            }
            else {
              this.filtri += "~'" + element.value + "')"
            }

          }
          if (element.matchMode == "lt") {
            this.filtri += "<" + element.value + ""
          }
          if (element.matchMode == "lte") {
            this.filtri += "<:" + element.value + ""
          }
          if (element.matchMode == "gt") {
            this.filtri += ">" + element.value + ""
          }
          if (element.matchMode == "gte") {
            this.filtri += ">:" + element.value + ""
          }

        }
        conta++;
      });
      this.filtri+=") and ";
     }
      

    });
    console.log("clear: ",this.filtri)
    if (this.filtri != "") {
      console.log("filter finale con and: ", this.filtri)
      this.filtri = this.filtri.slice(0, -5);
      this.filtri = "&filter=" + this.filtri;
      console.log("filter finale: ", this.filtri)
      
    }
    this.articoliService.getArticoli(this.filtri+"&size="+this.size+"&page="+this.page).subscribe((res => {
      this.lista = res.content;
      this.totalRecord = res.totalElements;
    }))


  }
  Inserisci(event){
    console.log(event)
    this.articoliService.Insert(event).subscribe(res=>{
      this.aggiornaTabella()
    })
  }
  Modifica(event){
    console.log(event)
    this.articoliService.Modifica(event).subscribe(res=>{
      this.aggiornaTabella()
    })


  }
  aggiornaTabella(){
    
    this.articoliService.getArticoli(this.filtri+"&size="+this.size+"&page="+this.page).subscribe((res => {
      this.lista = res.content;
      console.log(this.lista)
      this.totalRecord = res.totalElements;
    }))
  }


}
