import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ArticoliRoutingModule } from './articoli-routing.module';
import { ArticoliComponent } from './articoli/articoli.component';
import { CondiviseModule } from '../../condivise/condivise.module';
import { TabViewModule } from 'primeng/tabview';
import { DialogModule } from 'primeng/dialog';
import { ButtonModule } from 'primeng/button';

@NgModule({
  declarations: [
    ArticoliComponent
  ],
  imports: [
    CommonModule,
    ArticoliRoutingModule,CondiviseModule,TabViewModule,DialogModule,ButtonModule
  ]
})
export class ArticoliModule { }
