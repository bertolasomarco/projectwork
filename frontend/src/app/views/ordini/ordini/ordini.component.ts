import { Component, OnInit } from '@angular/core';
import { LazyLoadEvent } from 'primeng/api';
import { DataTableOptions } from '../../../../api/datatable-options';
import { ArticoliService } from '../../../../service/articoli.service';
import { OrdiniService } from '../../../../service/ordini.service';

@Component({
  selector: 'app-ordini',
  templateUrl: './ordini.component.html',
  styleUrls: ['./ordini.component.scss']
})
export class OrdiniComponent implements OnInit {
  lista: any[]
  lista2: any[]
  tabella: DataTableOptions;
  tabella2: DataTableOptions;
  mostraDialog: boolean= false;
  filtri: any = "";
  totalRecord = 0;
  page = 0;
  size = 10;
  infoSelect: any[];
  dati:any;
  titolo:any;
  index:number=0;

  constructor(public ordiniService: OrdiniService) { }

  ngOnInit(): void {

    this.tabella = {
      colsOptions: [
        { label: "AmazonOrderId", name: "Ordine ID", type: "text" },
        { label: "OrderStatus", name: "Stato", type: "text" },
        { label: "NumberOfItemsShipped", name: "Articoli Spediti", type: "numeric" },
        { label: "BuyerName", name: "Nome Acquirente", type: "text" },
        { label: "BuyerEmail", name: "Email Acquirente", type: "text" },
        //{ label: "Brand", name: "Brand", type: "text" }
      ],
    }
    this.tabella2 = {
      colsOptions: [
        { label: "asin", name: "ASIN", type: "text" },
        { label: "title", name: "NOME", type: "text" },
        { label: "brand", name: "Brand", type: "numeric" },
        { label: "prezzo", name: "Prezzo Unitario", type: "text" },
        { label: "quantity", name: "Quantità", type: "text" },
        //{ label: "Brand", name: "Brand", type: "text" }
      ],
    }

  }

  buttonDettClick(event){
    //Servizio LISTA2




    console.log(event.item)
    this.dati = event.item;
    this.titolo = "Informazioni sull'ordine: " + this.dati['AmazonOrderId'];
    //servizio
    this.ordiniService.getArticoliOrdine(this.dati['AmazonOrderId']).subscribe((res => {
      console.log(res)
      this.lista2 = res;
    }))

    this.infoSelect = [
      { name: 'Email cliente', value: this.dati['BuyerEmail'] },
      { name: 'Nome Cliente', value: this.dati['BuyerName'] },
      { name: 'Tipo di ordine', value: this.dati['OrderType'] },
      { name: 'Metodo di pagamento', value: this.dati['PaymentMethod'] },
      { name: 'Data Acquisto', value: this.dati['PurchaseDate'] },

    ];
    this.mostraDialog=true;
  }



  reset(){
    this.mostraDialog=false;
  }

  LoadArticoli(event: LazyLoadEvent) {
    // this.loading = true;
    //console.log(event)
    this.ordiniService.getOrdini(this.filtri+"&size="+this.size+"&page="+this.page).subscribe((res => {
      this.lista = res.content;
      this.totalRecord = res.totalElements;
    }))
  }
  Paginazione(event: any){
    console.log(event)

    this.page = event.first/event.rows
    this.size = event.rows

    this.ordiniService.getOrdini(this.filtri+"&size="+this.size+"&page="+this.page).subscribe((res => {
      this.lista = res.content;
      this.totalRecord = res.totalElements;
    }))
    
  }

  InserimentoFiltri(event: any) {
    this.filtri = ""
   // console.log("filtri ", event.filters);
    Object.keys(event.filters).forEach(element => {
     // console.log(element)
      var tipo = element;
     // console.log("VEDOOO: ",event.filters[element])
      var conta = 0;
     if(event.filters[element][0].value!=null){
      this.filtri+="(";
      event.filters[element].forEach(element => {
       // console.log("ELEMENTTTTTTTTTTTT: ", element)
        if (conta == 1) {
          this.filtri += " " + element.operator + " "
        }
        if (element.value != null) {
          if(element.matchMode == "notEquals"){
            this.filtri += "not("+tipo
          }
          else{
            this.filtri += tipo
          }
          
          if (element.matchMode == "startsWith") {
            this.filtri += "~'" + element.value + "*'"
          }
          if (element.matchMode == "contains") {
            this.filtri += "~'*" + element.value + "*'"
          }
          if (element.matchMode == "notContains") {
            this.filtri += "!~'*" + element.value + "*'"
          }
          if (element.matchMode == "endWith") {
            this.filtri += "~'*" + element.value + "'"
          }
          if (element.matchMode == "equals") {
            if (typeof element.value == "number") {
              this.filtri += ":" + element.value + ""
            }
            else {
              this.filtri += "~'" + element.value + "'"
            }

          }
          if (element.matchMode == "notEquals") {
            if (typeof element.value == "number") {
              this.filtri += ":" + element.value + ")"
            }
            else {
              this.filtri += "~'" + element.value + "')"
            }

          }
          if (element.matchMode == "lt") {
            this.filtri += "<" + element.value + ""
          }
          if (element.matchMode == "lte") {
            this.filtri += "<:" + element.value + ""
          }
          if (element.matchMode == "gt") {
            this.filtri += ">" + element.value + ""
          }
          if (element.matchMode == "gte") {
            this.filtri += ">:" + element.value + ""
          }

        }
        conta++;
      });
      this.filtri+=") and ";
     }
      

    });
    console.log("clear: ",this.filtri)
    if (this.filtri != "") {
      console.log("filter finale con and: ", this.filtri)
      this.filtri = this.filtri.slice(0, -5);
      this.filtri = "&filter=" + this.filtri;
      console.log("filter finale: ", this.filtri)
      
    }
    this.ordiniService.getOrdini(this.filtri+"&size="+this.size+"&page="+this.page).subscribe((res => {
      this.lista = res.content;
      this.totalRecord = res.totalElements;
    }))


  }


}
