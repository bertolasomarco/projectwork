import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { OrdiniRoutingModule } from './ordini-routing.module';
import { OrdiniComponent } from './ordini/ordini.component';
import { CondiviseModule } from '../../condivise/condivise.module';
import { TabViewModule } from 'primeng/tabview';
import { DialogModule } from 'primeng/dialog';
import { ButtonModule } from 'primeng/button';


@NgModule({
  declarations: [
    OrdiniComponent
  ],
  imports: [
    CommonModule,
    OrdiniRoutingModule,CondiviseModule,TabViewModule,DialogModule,ButtonModule
  ]
})
export class OrdiniModule { }
