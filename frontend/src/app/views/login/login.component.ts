import { Component } from '@angular/core';
import { UtenzaService } from '../../../service/utenza.service';
import { Router } from '@angular/router';
import { AuthenticationService } from '../../../auth/authentication.service';
 
@Component({
  selector: 'app-dashboard',
  templateUrl: 'login.component.html'
})
export class LoginComponent {
  public user: string = "";
  public pass: string = "";
  errato : boolean = false;
  constructor(public auth: AuthenticationService,public utenzaService: UtenzaService, public router: Router) { }
 
  ngOnInit(): void {
    window.sessionStorage.clear();
    this.errato = false;
    this.auth.logout();
  }
 
  VerificaLogin() {
    this.auth.login({ username: this.user, password: this.pass}).subscribe((res) => {
      console.log(res)
      if (res.error != null) 
        this.errato = true;
      else {
        this.errato = false;
        window.sessionStorage.setItem("jwt", res.jwt);
        this.utenzaService.SetControllo("0")
        this.router.navigate(['/grafici']);
      }
    });
  }
}