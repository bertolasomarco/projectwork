import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {ButtonModule} from 'primeng/button';

import { AcquistiRoutingModule } from './acquisti-routing.module';
import { AcquistiComponent } from './acquisti/acquisti.component';
import { DialogModule } from 'primeng/dialog';
import { TabViewModule } from 'primeng/tabview';
import { CondiviseModule } from '../../condivise/condivise.module';



@NgModule({
  declarations: [
    AcquistiComponent
  ],
  imports: [
    CommonModule,
    AcquistiRoutingModule,ButtonModule,CondiviseModule,TabViewModule,DialogModule,ButtonModule
  ]
})
export class AcquistiModule { }
