import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AcquistiComponent } from './acquisti/acquisti.component';

const routes: Routes = [
  {
    path: '',
    component: AcquistiComponent,
    data: {
      title: 'Acquisti'
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AcquistiRoutingModule { }
