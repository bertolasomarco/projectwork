import { Component, OnInit } from '@angular/core';
import { LazyLoadEvent } from 'primeng/api';
import { timeout } from 'rxjs/operators';
import { DataTableOptions } from '../../../../api/datatable-options';
import { AcquistiService } from '../../../../service/acquisti.service';
import { FornitoriService } from '../../../../service/fornitori.service';

@Component({
  selector: 'app-acquisti',
  templateUrl: './acquisti.component.html',
  styleUrls: ['./acquisti.component.scss']
})
export class AcquistiComponent implements OnInit {
  mostra: boolean = true;
  mostra2: boolean = false;
  tipodialog: string= "acquisto";
  lista: any[]=[];
  lista2:any[]=[];
  lunghezza: any[];
  tabella: DataTableOptions;
  nominativo: any;
  index: any;
  mostraDialog: boolean= false;
  mostraDialog2: boolean= false;
  mostraDialog3: boolean= false;
  fattura : any;
  nuovo:boolean;
  idA:any;
  idElimina:any;
  eseguita:boolean;
  InfoSel:any[];
  listaArticoli:any[]

  datiu: any[];
  mostraDialogDelete : boolean = false;
  nomeelimina: any;
  fonelimina: any;

  filtri: any = "";
  totalRecord = 0;
  page = 0;
  size = 10;
  
  constructor(public acquistiService : AcquistiService,public fornitoriService: FornitoriService) { }

  
  ngOnInit(): void {
    this.InfoSel = [
      { name: 'Nome Fornitore', value: ""},
      { name: 'Numero Fattura', value: ""},
      { name: 'Data Fattura', value: null },
      { name: 'Spesa Totale', value: "" },
      { name: 'ID', value: 0 }
    ];
    this.acquistiService.getAcquisti(this.filtri+"&size="+this.size+"&page="+this.page).subscribe((res => {
      res.content.forEach(element => {
          this.acquistiService.totSpesa(element.acquistoID).subscribe((res => {
            this.lista.push(
              {
                "acquistoID":element.acquistoID,
                "dataFattura":element.dataFattura,
                "effettuata":element.effettuata,
                "idfornitore":element.fornitore.fornitoriID,
                "numeroFattura":element.numeroFattura,
                "fornitore": element.fornitore.nome,
                "totale" : res.totale
              })
              

          
        }))
        
      });
      this.totalRecord = res.totalElements;
    }))

    this.tabella = {
      colsOptions: [
        { label: "numeroFattura", name: "Numero Fattura", type: "text" },
        { label: "fornitore", name: "Fornitore", type: "text" },
        { label: "dataFattura", name: "Data Fattura", type: "date" },
        { label: "totale", name: "Spesa Totale", type: "numeric" }
      ],
    }
    

  }


  nascondi() {
    this.mostraDialog = false;
  }
  nascondi2() {
    this.mostraDialog2 = false;
  }
  nascondi3() {
    this.mostraDialog3 = false;
  }
  CheckFattura(event) {
    this.nominativo= "Inserire la fattura per l'acquisto "+event.item.numeroFattura+" ?"
    this.datiu = event.item;
    this.idA = event.item.acquistoID
    this.InfoSel = [
      { name: 'Nome Fornitore', value:this.datiu['fornitore']},
      { name: 'Numero Fattura', value: this.datiu['numeroFattura'] },
      { name: 'Data Fattura', value: this.datiu['dataFattura'] },
      { name: 'Spesa Totale', value: this.datiu['totale'] },
      { name: 'ID', value: this.datiu['acquistoID'] }
    ];
    this.listaArticoli=[]
    this.acquistiService.listaArticoli(this.datiu['acquistoID']).subscribe(res=>{
      
      res.forEach(element => {
        console.log(element)
        this.listaArticoli.push({
          "asin":element.items.asin,
          "title":element.items.title,
          "ItemID":element.items.ItemID,
          "quantita":element.qauantitaAcquistata,
          "prezzo":element.PrezzoUnitarioAcquisto
        })
        
      });
      console.log(this.listaArticoli)
    })
    this.index = 0;
    this.mostraDialog2 = true;

  }
  buttonEliminaClick(event){
    this.nomeelimina= event.item.numeroFattura
    this.idElimina= event.item.acquistoID
    
    this.mostraDialogDelete=true
  }
  DettaglioAcquisto(event){
    console.log(event.item)
    this.eseguita=false;
    this.fattura=false;
    this.index = 0;
    this.datiu = event.item;
    console.log(this.InfoSel)
    if(event.item.numfattura=="Non selezionato"){
      this.nominativo= "Modifica Acquisto";
      
      console.log("datiu",this.datiu)
      this.InfoSel = [
        { name: 'Nome Fornitore', value: {label:this.datiu['fornitore'],value:this.datiu['idfornitore']}},
        { name: 'Numero Fattura', value: this.datiu['numeroFattura'] },
        { name: 'Data Fattura', value: new Date( this.datiu['dataFattura'] )},
        { name: 'Spesa Totale', value: this.datiu['totale'] }
      ];
    }
    else{
      this.nominativo= "Modifica Acquisto, Numero Fattura: "+event.item.numeroFattura;
      console.log("datiu",this.datiu)
      this.InfoSel = [
        { name: 'Nome Fornitore', value: {label:this.datiu['fornitore'],value:this.datiu['idfornitore']}},
        { name: 'Numero Fattura', value: this.datiu['numeroFattura'] },
        { name: 'Data Fattura', value:new Date( this.datiu['dataFattura'] ) },
        { name: 'Spesa Totale', value: this.datiu['totale'] }
      ];
    }
    this.listaArticoli=[]
    this.acquistiService.listaArticoli(this.datiu['acquistoID']).subscribe(res=>{
      res.forEach(element => {
        this.listaArticoli.push({
          "asin":element.items.asin,
          "title":element.items.title,
          "ItemID":element.items.ItemID,
          "quantita":element.qauantitaAcquistata,
          "prezzo":element.PrezzoUnitarioAcquisto
        })
        
      });
      console.log(this.listaArticoli)
    })
    console.log("lista articoli :", this.listaArticoli)
    this.idA = event.item.acquistoID
    this.mostraDialog3=true
  }
  buttonVediAClick2(event){
    this.datiu = event.item;
    this.eseguita=true
    this.fattura=true;
    this.index = 0;
    this.nominativo= "Fattura Numero: "+event.item.numeroFattura
    this.InfoSel = [
      { name: 'Nome Fornitore', value: this.datiu['fornitore']},
      { name: 'Numero Fattura', value: this.datiu['numeroFattura'] },
      { name: 'Data Fattura', value: this.datiu['dataFattura'] },
      { name: 'Spesa Totale', value: this.datiu['totale'] }
    ];
    this.idA = event.item.acquistoID

    this.listaArticoli=[]
    this.acquistiService.listaArticoli(this.idA).subscribe(res=>{
      res.forEach(element => {
        this.listaArticoli.push({
          "asin":element.items.asin,
          "title":element.items.title,
          "ItemID":element.items.ItemID,
          "quantita":element.qauantitaAcquistata,
          "prezzo":element.PrezzoUnitarioAcquisto
        })
        
      });
      console.log(this.listaArticoli)
    })
    this.mostraDialog3=true
  }


  EliminaAcquisto(event: any){
    console.log("vedo ",this.idElimina)
    this.acquistiService.elimina(this.idElimina).subscribe(res=>{
      this.lista=[]
      this.acquistiService.getAcquisti(this.filtri+"&size="+this.size+"&page="+this.page).subscribe((res => {
        res.content.forEach(element => {
          
            this.acquistiService.totSpesa(element.acquistoID).subscribe((res => {
              this.lista.push(
                {
                  "acquistoID":element.acquistoID,
                  "dataFattura":element.dataFattura,
                  "effettuata":element.effettuata,
                  "idfornitore":element.fornitore.id,
                  "numeroFattura":element.numeroFattura,
                  "fornitore": element.fornitore.nome,
                  "totale" : res.totale
                })
  
            
          }))
          
        });
        this.totalRecord = res.totalElements;
      }))
    })
    this.mostraDialogDelete=false
  }


  VediAgg(event){
    this.nuovo=true
    this.eseguita=false;
    this.fattura=false;
    this.index = 0;
    this.idA= "NUOVO"
    this.InfoSel = [
      { name: 'Nome Fornitore', value: {label:"",value:""}},
      { name: 'Numero Fattura', value: ""},
      { name: 'Data Fattura', value:  null},
      { name: 'Spesa Totale', value:  "" },
      { name: 'ID', value: 0 }
    ];
    this.listaArticoli=[]
    this.mostraDialog3 = true;
    this.nominativo = "Inserisci un nuovo acquisto";
    
  }

  LoadAcquisti(event: LazyLoadEvent) {
    // this.loading = true;
    //console.log(event)
    this.lista=[]
      this.acquistiService.getAcquisti("&filter=(effettuata:false)"+this.filtri+"&size="+this.size+"&page="+this.page).subscribe((res => {
        res.content.forEach(element => {
          
            this.acquistiService.totSpesa(element.acquistoID).subscribe((res => {
              this.lista.push(
                {
                  "acquistoID":element.acquistoID,
                  "dataFattura":element.dataFattura,
                  "effettuata":element.effettuata,
                  "idfornitore":element.fornitore.id,
                  "numeroFattura":element.numeroFattura,
                  "fornitore": element.fornitore.nome,
                  "totale" : res.totale
                })
  
            
          }))
          
        });
        this.totalRecord = res.totalElements;
      }))
  }
  Paginazione(event: any){
    console.log(event)

    this.page = event.first/event.rows
    this.size = event.rows

    this.acquistiService.getAcquisti("&filter=(effettuata:false)"+this.filtri+"&size="+this.size+"&page="+this.page).subscribe((res => {
      this.lista = res.content;
      this.totalRecord = res.totalElements;
    }))
    
  }

  InserimentoFiltri(event: any) {
    this.filtri = ""
   // console.log("filtri ", event.filters);
    Object.keys(event.filters).forEach(element => {
     // console.log(element)
      var tipo = element;
     // console.log("VEDOOO: ",event.filters[element])
      var conta = 0;
     if(event.filters[element][0].value!=null){
      this.filtri+="(";
      event.filters[element].forEach(element => {
       // console.log("ELEMENTTTTTTTTTTTT: ", element)
        if (conta == 1) {
          this.filtri += " " + element.operator + " "
        }
        if (element.value != null) {
          if(element.matchMode == "notEquals"){
            this.filtri += "not("+tipo
          }
          else{
            this.filtri += tipo
          }
          
          if (element.matchMode == "startsWith") {
            this.filtri += "~'" + element.value + "*'"
          }
          if (element.matchMode == "contains") {
            this.filtri += "~'*" + element.value + "*'"
          }
          if (element.matchMode == "notContains") {
            this.filtri += "!~'*" + element.value + "*'"
          }
          if (element.matchMode == "endWith") {
            this.filtri += "~'*" + element.value + "'"
          }
          if (element.matchMode == "equals") {
            if (typeof element.value == "number") {
              this.filtri += ":" + element.value + ""
            }
            else {
              this.filtri += "~'" + element.value + "'"
            }

          }
          if (element.matchMode == "notEquals") {
            if (typeof element.value == "number") {
              this.filtri += ":" + element.value + ")"
            }
            else {
              this.filtri += "~'" + element.value + "')"
            }

          }
          if (element.matchMode == "lt") {
            this.filtri += "<" + element.value + ""
          }
          if (element.matchMode == "lte") {
            this.filtri += "<:" + element.value + ""
          }
          if (element.matchMode == "gt") {
            this.filtri += ">" + element.value + ""
          }
          if (element.matchMode == "gte") {
            this.filtri += ">:" + element.value + ""
          }

        }
        conta++;
      });
      this.filtri+=") and ";
     }
      

    });
    console.log("clear: ",this.filtri)
    if (this.filtri != "") {
      this.filtri = this.filtri.slice(0, -5);
      this.filtri = " and " + this.filtri;
      
    }
    this.lista=[]
      this.acquistiService.getAcquisti("&filter=(effettuata:false)"+this.filtri+"&size="+this.size+"&page="+this.page).subscribe((res => {
        res.content.forEach(element => {
          
            this.acquistiService.totSpesa(element.acquistoID).subscribe((res => {
              this.lista.push(
                {
                  "acquistoID":element.acquistoID,
                  "dataFattura":element.dataFattura,
                  "effettuata":element.effettuata,
                  "idfornitore":element.fornitore.id,
                  "numeroFattura":element.numeroFattura,
                  "fornitore": element.fornitore.nome,
                  "totale" : res.totale
                })
  
            
          }))
          
        });
        this.totalRecord = res.totalElements;
      }))


  }
  LoadAcquisti2(event: LazyLoadEvent) {
    // this.loading = true;
    //console.log(event)
    this.lista2=[]
    this.acquistiService.getAcquisti("&filter=(effettuata:true)"+this.filtri+"&size="+this.size+"&page="+this.page).subscribe((res => {
        res.content.forEach(element => {
          
            this.acquistiService.totSpesa(element.acquistoID).subscribe((res => {
              this.lista2.push(
                {
                  "acquistoID":element.acquistoID,
                  "dataFattura":element.dataFattura,
                  "effettuata":element.effettuata,
                  "idfornitore":element.fornitore.id,
                  "numeroFattura":element.numeroFattura,
                  "fornitore": element.fornitore.nome,
                  "totale" : res.totale
                })
  
            
          }))
          
        });
        this.totalRecord = res.totalElements;
      }))
  }
  Paginazione2(event: any){
    console.log(event)

    this.page = event.first/event.rows
    this.size = event.rows

    this.lista2=[]
    this.acquistiService.getAcquisti("&filter=(effettuata:true)"+this.filtri+"&size="+this.size+"&page="+this.page).subscribe((res => {
      res.content.forEach(element => {
        
          this.acquistiService.totSpesa(element.acquistoID).subscribe((res => {
            this.lista2.push(
              {
                "acquistoID":element.acquistoID,
                "dataFattura":element.dataFattura,
                "effettuata":element.effettuata,
                "idfornitore":element.fornitore.id,
                "numeroFattura":element.numeroFattura,
                "fornitore": element.fornitore.nome,
                "totale" : res.totale
              })

          
        }))
        
      });
      this.totalRecord = res.totalElements;
    }))
    
  }

  InserimentoFiltri2(event: any) {
    this.filtri = ""
   // console.log("filtri ", event.filters);
    Object.keys(event.filters).forEach(element => {
     // console.log(element)
      var tipo = element;
     // console.log("VEDOOO: ",event.filters[element])
      var conta = 0;
     if(event.filters[element][0].value!=null){
      this.filtri+="(";
      event.filters[element].forEach(element => {
       // console.log("ELEMENTTTTTTTTTTTT: ", element)
        if (conta == 1) {
          this.filtri += " " + element.operator + " "
        }
        if (element.value != null) {
          if(element.matchMode == "notEquals"){
            this.filtri += "not("+tipo
          }
          else{
            this.filtri += tipo
          }
          
          if (element.matchMode == "startsWith") {
            this.filtri += "~'" + element.value + "*'"
          }
          if (element.matchMode == "contains") {
            this.filtri += "~'*" + element.value + "*'"
          }
          if (element.matchMode == "notContains") {
            this.filtri += "!~'*" + element.value + "*'"
          }
          if (element.matchMode == "endWith") {
            this.filtri += "~'*" + element.value + "'"
          }
          if (element.matchMode == "equals") {
            if (typeof element.value == "number") {
              this.filtri += ":" + element.value + ""
            }
            else {
              this.filtri += "~'" + element.value + "'"
            }

          }
          if (element.matchMode == "notEquals") {
            if (typeof element.value == "number") {
              this.filtri += ":" + element.value + ")"
            }
            else {
              this.filtri += "~'" + element.value + "')"
            }

          }
          if (element.matchMode == "lt") {
            this.filtri += "<" + element.value + ""
          }
          if (element.matchMode == "lte") {
            this.filtri += "<:" + element.value + ""
          }
          if (element.matchMode == "gt") {
            this.filtri += ">" + element.value + ""
          }
          if (element.matchMode == "gte") {
            this.filtri += ">:" + element.value + ""
          }

        }
        conta++;
      });
      this.filtri+=") and ";
     }
      

    });
    console.log("clear: ",this.filtri)
    if (this.filtri != "") {
      console.log("filter finale con and: ", this.filtri)
      this.filtri = " and " + this.filtri;
      
    }
    this.lista2=[]
    this.acquistiService.getAcquisti("&filter=(effettuata:true)"+this.filtri+"&size="+this.size+"&page="+this.page).subscribe((res => {
      res.content.forEach(element => {
        
          this.acquistiService.totSpesa(element.acquistoID).subscribe((res => {
            this.lista2.push(
              {
                "acquistoID":element.acquistoID,
                "dataFattura":element.dataFattura,
                "effettuata":element.effettuata,
                "idfornitore":element.fornitore.id,
                "numeroFattura":element.numeroFattura,
                "fornitore": element.fornitore.nome,
                "totale" : res.totale
              })

          
        }))
        
      });
      this.totalRecord = res.totalElements;
    }))


  }
  AggiornaTabelle(){
    
    this.acquistiService.getAcquisti("&filter=(effettuata:false)"+this.filtri+"&size="+this.size+"&page="+this.page).subscribe((res => {
      res.content.forEach(element => {
          this.acquistiService.totSpesa(element.acquistoID).subscribe((res => {
            this.lista.push(
              {
                "acquistoID":element.acquistoID,
                "dataFattura":element.dataFattura,
                "effettuata":element.effettuata,
                "idfornitore":element.fornitore.fornitoriID,
                "numeroFattura":element.numeroFattura,
                "fornitore": element.fornitore.nome,
                "totale" : res.totale
              })
              

          
        }))
        
      });
      this.totalRecord = res.totalElements;
    }))
    this.acquistiService.getAcquisti("&filter=(effettuata:true)"+this.filtri+"&size="+this.size+"&page="+this.page).subscribe((res => {
      res.content.forEach(element => {
          this.acquistiService.totSpesa(element.acquistoID).subscribe((res => {
            this.lista2.push(
              {
                "acquistoID":element.acquistoID,
                "dataFattura":element.dataFattura,
                "effettuata":element.effettuata,
                "idfornitore":element.fornitore.fornitoriID,
                "numeroFattura":element.numeroFattura,
                "fornitore": element.fornitore.nome,
                "totale" : res.totale
              })
              

          
        }))
        
      });
      this.totalRecord = res.totalElements;
    }))
  }

}
