import { Component, OnInit } from '@angular/core';
import { GraficiService } from '../../../../service/grafici.service';

interface Grafici {
  name: string,
  value: string
}

@Component({
  selector: 'app-grafici',
  templateUrl: './grafici.component.html',
  styleUrls: ['./grafici.component.scss']
})
export class GraficiComponent implements OnInit {
  
  graficoaBarreSettQta: any;
  optionsGraficoaBarreSettQta: any;

  graficoaBarreSettRev: any;
  optionsGraficoaBarreSettRev: any;

  graficoaTorta: any;
  optionsGraficoaTorta: any;

  graficoaBarre: any;
  optionsGraficoaBarre: any;

  graficoaBarreOrizzontale: any;
  optionsGraficoaBarreOrizzontale: any;

  items: Grafici[];
  selectedItem: Grafici;
  categoria: Grafici[];
  selectedCategoria: Grafici;

  changeValueItem(event){
    console.log(event);
    console.log(this.selectedItem);

    let color = '#' + (0x1000000 + Math.random() * 0xffffff).toString(16).substr(1, 6);
    let color2 = '#' + (0x1000000 + Math.random() * 0xffffff).toString(16).substr(1, 6);

    this.serviceGrafici.getQtaSoldItemsWeek(this.selectedItem.value).subscribe((res) => {

      this.graficoaBarre = {
        labels: [],
        datasets: [
            {
                label: "Qtà vendute",
                backgroundColor: color,
                data: []
            },
            {
              label: "Totale ricavi (in EUR)",
              backgroundColor: color2,
              data: []
            }
        ]
      };
      res.forEach(element => {
       this.graficoaBarre.labels.push(element.week);
       this.graficoaBarre.datasets[0].data.push(element.quantity);
       this.graficoaBarre.datasets[1].data.push(element.revenues);
      });
    })

  }

  changeValueCategoria(event){

    let color = '#' + (0x1000000 + Math.random() * 0xffffff).toString(16).substr(1, 6);
    let color2 = '#' + (0x1000000 + Math.random() * 0xffffff).toString(16).substr(1, 6);


    this.serviceGrafici.getQtaSoldCategoryWeek(this.selectedCategoria.name).subscribe(resQtaSoldCategoryWeek => {

      this.graficoaBarreOrizzontale = {
        labels: [],
        datasets: [
          {
            label: ['Qtà vendute'],
            backgroundColor: [],
            data: []          
          },
          {
            label: ['Totale ricavi (in EUR)'],
            backgroundColor: [],
            data: []          
          }
        ]
      };

      resQtaSoldCategoryWeek.map(i => {
        this.graficoaBarreOrizzontale.labels.push(i.week);
        let color = '#' + (0x1000000 + Math.random() * 0xffffff).toString(16).substr(1, 6)
        let color2 = '#' + (0x1000000 + Math.random() * 0xffffff).toString(16).substr(1, 6)

        this.graficoaBarreOrizzontale.datasets[0].backgroundColor.push(color);
        this.graficoaBarreOrizzontale.datasets[1].backgroundColor.push(color2);
        if(i.quantity > 0){
          this.graficoaBarreOrizzontale.datasets[0].data.push(i.quantity);
          this.graficoaBarreOrizzontale.datasets[1].data.push(i.revenues);
        }
    });

    this.graficoaBarreOrizzontale.datasets[0].data.push(0);
    });
  }

  constructor(public serviceGrafici: GraficiService) {

    this.items = [];
    this.serviceGrafici.getGraficoaBarre().subscribe(res => {

      res.content.forEach(e => {
        this.items.push({name: e.title, value: e.asin});
      });

    this.items = this.items;

    this.categoria = [
      {name: 'Maglia Calcio', value:"Maglia Calcio"},
      {name: 'Videogiochi', value:"Videogiochi"},
      {name: 'Palestra', value:"Palestra"},
      {name: 'Bibite', value:"Bibite"},
      {name: 'Giochi', value:"Giochi"},
      {name: 'Illuminazione', value:"Illuminazione"},
    ];

  })

  }
  
  ngOnInit(): void {

    this.serviceGrafici.getGraficoaBarreSett().subscribe(resBarreSettQta => {

      this.graficoaBarreSettQta = {
        labels: [],
        datasets: [
          {
            label: ['Qtà vendute'],
            backgroundColor: [],
            data: []          
          }
        ]
      };

      resBarreSettQta.map(i => {
        
          this.graficoaBarreSettQta.labels.push(i.week);

          let color = '#' + (0x1000000 + Math.random() * 0xffffff).toString(16).substr(1, 6)
          
          this.graficoaBarreSettQta.datasets[0].backgroundColor.push(color);
          if(i.quantity > 0){
            this.graficoaBarreSettQta.datasets[0].data.push(i.quantity);
          }  
      });

      this.graficoaBarreSettQta.datasets[0].data.push(0);

      this.optionsGraficoaBarreSettQta = {
        responsive: false,
          title: {
            display: false,
            text: 'Quantità totali vendute per settimana',
            fontSize: 25
          },
          legend: {
            display: false,
            position: 'top',
            align: 'center'
          },
      };

    });
    

    this.serviceGrafici.getGraficoaBarreSett().subscribe(resBarreSettRev => {

      this.graficoaBarreSettRev = {
        labels: [],
        datasets: [
          {
            label: ['Totale ricavi'],
            backgroundColor: [],
            data: []          
          }
        ]
      };

      resBarreSettRev.map(i => {
        
          this.graficoaBarreSettRev.labels.push(i.week);

          let color = '#' + (0x1000000 + Math.random() * 0xffffff).toString(16).substr(1, 6)
          
          this.graficoaBarreSettRev.datasets[0].backgroundColor.push(color);
          if(i.revenues > 0){
            this.graficoaBarreSettRev.datasets[0].data.push(i.revenues);
          }  
      });

      this.graficoaBarreSettRev.datasets[0].data.push(0);

      this.optionsGraficoaBarreSettRev = {
        responsive: false,
          title: {
            display: false,
            text: 'Totale ricavi per settimana',
            fontSize: 25
          },
          legend: {
            display: false,
            position: 'top',
            align: 'center'
          },
      };

    });

    this.serviceGrafici.getGraficoaTorta().subscribe(resTorta => {

      this.graficoaTorta = {
        labels: [],
        datasets: [
          {
            data: [],
            backgroundColor: [],
            hoverBackgroundColor: []
          }
        ],
        options: {
          tooltips: {
              callbacks: {
                  label: function(tooltipItem, data) {
                      var label = data.labels[tooltipItem.index];
                      return label;
                  }
              }
          }
      }
      };

      resTorta.map(i => {
        if (i.sold > 0) {
          this.graficoaTorta.labels.push(i.title.substr(0, 15) + "...");
          this.graficoaTorta.datasets[0].data.push(i.sold);
          let color = '#' + (0x1000000 + Math.random() * 0xffffff).toString(16).substr(1, 6);
          this.graficoaTorta.datasets[0].backgroundColor.push(color);
          this.graficoaTorta.datasets[0].hoverBackgroundColor.push(color);
        }
      });

      this.optionsGraficoaTorta = {
        responsive: false,
        title: {
          display: false,
          text: 'Articoli venduti',
          fontSize: 25
        },
        legend: {
          display: true,
          position: 'bottom',
          align: 'left'
        },

      };

    });


  }

}
