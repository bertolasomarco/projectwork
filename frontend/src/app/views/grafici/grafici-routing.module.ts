import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';
import { ChartModule } from 'primeng/chart';
import { GraficiComponent } from './grafici/grafici.component';

const routes: Routes = [
  {
    path: '',
    component: GraficiComponent,
    data: {
      title: 'Grafici'
    }
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(routes),
    FormsModule,
    ChartModule
  ],
  exports: [RouterModule]
})
export class GraficiRoutingModule { }
