import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { GraficiRoutingModule } from './grafici-routing.module';
import { GraficiComponent } from './grafici/grafici.component';
import { ChartModule } from 'primeng/chart';
import { ChartsModule } from 'ng2-charts';
import { DropdownModule } from 'primeng/dropdown';
import { FormsModule } from '@angular/forms';
@NgModule({
  declarations: [
    GraficiComponent
  ],
  imports: [
    CommonModule,
    GraficiRoutingModule,
    ChartModule,
    ChartsModule,
    DropdownModule,
    FormsModule
  ]
})

export class GraficiModule { }
