import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TabellaArticoliComponent } from './tabella-articoli.component';

describe('TabellaArticoliComponent', () => {
  let component: TabellaArticoliComponent;
  let fixture: ComponentFixture<TabellaArticoliComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TabellaArticoliComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TabellaArticoliComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
