import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { DataTableOptions } from '../../../api/datatable-options';

@Component({
  selector: 'app-tabella-articoli',
  templateUrl: './tabella-articoli.component.html',
  styleUrls: ['./tabella-articoli.component.scss']
})
export class TabellaArticoliComponent implements OnInit {
  @Input() lista: any[];
  @Input() public options: DataTableOptions;
  @Input() btnAcqArt1: boolean;
  

  @Output() onButtonEliminaClick: EventEmitter<any> = new EventEmitter<any>();
  constructor() { }

  ngOnInit(): void {
  }

  Elimina(item: any) {
    this.onButtonEliminaClick.emit({ item: item });
  }

}
