import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ArticoliacquistiComponent } from './articoliacquisti.component';

describe('ArticoliacquistiComponent', () => {
  let component: ArticoliacquistiComponent;
  let fixture: ComponentFixture<ArticoliacquistiComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ArticoliacquistiComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ArticoliacquistiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
