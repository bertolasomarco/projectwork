import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { DataTableOptions } from '../../../api/datatable-options';
import { AcquistiService } from '../../../service/acquisti.service';
import { ArticoliService } from '../../../service/articoli.service';
import { FornitoriService } from '../../../service/fornitori.service';

@Component({
  selector: 'app-articoliacquisti',
  templateUrl: './articoliacquisti.component.html',
  styleUrls: ['./articoliacquisti.component.scss']
})
export class ArticoliacquistiComponent implements OnInit {
  @Input() idAcquisto: any;
  @Input() titolo: any;
  @Input() index: any=0;
  @Input() tipo: string;
  @Input() eseguita: boolean;
  @Input() mostraDialog: boolean = false;
  @Input() informazioni: any[];
  @Input() fattura: any;
  @Input() lista: any[]=[];
  tabella: DataTableOptions;
  @Output() aggiornaAcquisti: EventEmitter<any> = new EventEmitter<any>();
  @Output() onHideDialog: EventEmitter<any> = new EventEmitter<any>();
  testo: any[];
  risultati: any[];
  risultati2:any[];

  costoarticolo: any = 0;
  quantitaarticolo: any = 0;
  
  numerof: any;
  dataf: any;



  handleDropdown(event) {
    //event.query = current value in input field
  }

  constructor(public articoliService : ArticoliService,public fornitoriService : FornitoriService,public acquistiService : AcquistiService) { }

  ngOnInit(): void {

    

    this.tabella = {
      colsOptions: [
        { label: "asin", name: "ASIN", type: "text" },
        { label: "title", name: "Nome", type: "text" },
        { label: "prezzo", name: "Prezzo", type: "numeric" },
        { label: "quantita", name: "Quantità", type: "numeric" }
        //{ label: "Brand", name: "Brand", type: "text" }
      ],
    }

    
  }


  hide() {
    this.costoarticolo = 0;
    this.quantitaarticolo = 0;
    this.onHideDialog.emit();
    this.testo=[]
    this.risultati=[]
    this.risultati2=[]
    this.lista=[]

  }
  trovaArticoli(event) {
    //SERVIZIO BACKEND
    let filtered : any[] = [];
        let query = event.query;

        this.articoliService.getArticoli("&filter=title~'*"+query+"*'").subscribe((res => {
          res.content.forEach(element => {
            filtered.push({label: element.title, value: element.ItemID,asin:element.asin})
          });
          this.risultati = filtered;
        }))
  }

  trovaFornitori(event){
    //SERVIZIO BACKEND
    let filtered : any[] = [];
        let query = event.query;

        this.fornitoriService.getFornitori("&filter=nome~'*"+query+"*'").subscribe((res => {
          res.content.forEach(element => {
            filtered.push({label: element.nome, value: element.fornitoriID})
          });
          this.risultati2 = filtered;
        }))
  }


  AggiungiArticolo() {
    console.log(this.informazioni)
    console.log(this.testo)
    this.lista.push({
      "ItemID":this.testo["value"],
      "AcquistoItemID": this.idAcquisto,
      "title": this.testo["label"],
      "asin": this.testo["asin"],
      "quantita": this.quantitaarticolo,
      "prezzo": this.costoarticolo
    })
    console.log(this.idAcquisto)

  }

  buttonEliminaClick(event) {
    console.log(event)

    this.lista = this.lista.filter(val => {
      if(val != event.item){
        return val
      }
      });
  }

  EseguiTutto(){
    console.log("per id fornitore",this.informazioni)
    if(this.idAcquisto=="NUOVO"){
      console.log("inserisci il nuovo acquisto")
      
      this.informazioni[3]=0;
      if(this.lista.length!=0){
        this.lista.forEach(element => {
          this.informazioni[3]+=element.quantitaarticolo * element.costoarticolo
        });
      }
      var listaart=[]
      this.lista.forEach(element => {
        listaart.push({
          acquistoID:           null,
          items:{ItemID:element.ItemID},
          qauantitaAcquistata:  element.quantita,
          prezzoUnitarioAcquisto: element.prezzo})
      });
    console.log({"acquisto": listaart, "fattura":{
      fornitore:{fornitoriID:this.informazioni[0].value.value},
      dataFattura:this.informazioni[2].value,
      numeroFattura:this.informazioni[1].value}
    })
      //servizio inserimento acquisto e lista
      this.acquistiService.insert({"acquisto": listaart, "fattura":{
        fornitore:{fornitoriID:this.informazioni[0].value.value},
        dataFattura:this.informazioni[2].value,
        numeroFattura:this.informazioni[1].value}
      }).subscribe((res) => {

      });
      this.mostraDialog=false;


    }
    else{
      console.log("Modifica acquisto")
      this.informazioni[3]=0;
      if(this.lista.length!=0){
        this.lista.forEach(element => {
          this.informazioni[3]+=element.quantitaarticolo * element.costoarticolo
        });
      }

      var listaart=[]
      this.lista.forEach(element => {
        
        listaart.push({acquistoid:this.idAcquisto,items:{ItemID:element.ItemID},
          qauantitaAcquistata:element.quantita,
          prezzoUnitarioAcquisto:element.prezzo})
      });
        //servizio inserimento acquisto e lista
      this.acquistiService.modifica({acquisto: listaart,fattura :{acquistoID:this.idAcquisto,fornitore:{fornitoriID:this.informazioni[0].value.value},dataFattura:this.informazioni[2].value,numeroFattura:this.informazioni[1].value}}).subscribe((res) => {

      });
      this.mostraDialog=false;
    }


    this.aggiornaAcquisti.emit();

  }
}
