import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CheckFatturaComponent } from './check-fattura.component';

describe('CheckFatturaComponent', () => {
  let component: CheckFatturaComponent;
  let fixture: ComponentFixture<CheckFatturaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CheckFatturaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CheckFatturaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
