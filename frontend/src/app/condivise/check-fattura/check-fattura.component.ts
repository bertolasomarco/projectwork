import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { listenerCount } from 'events';
import { DataTableOptions } from '../../../api/datatable-options';
import { AcquistiService } from '../../../service/acquisti.service';

@Component({
  selector: 'app-check-fattura',
  templateUrl: './check-fattura.component.html',
  styleUrls: ['./check-fattura.component.scss']
})
export class CheckFatturaComponent implements OnInit {
  @Input() idAcquisto: any;
  @Input() titolo: any;
  @Input() index: any;
  @Input() mostraDialog: boolean = false;
  @Input() informazioni: any[];
  @Input() lista: any[]=[];
  tabella: DataTableOptions;
  numerof:any;
  dataf:any;
  @Output() onHideDialog: EventEmitter<any> = new EventEmitter<any>();

  constructor(public acquistiService : AcquistiService) { }
 

  hide() {
    this.onHideDialog.emit();
  }
  ngOnInit(): void {
    this.tabella = {
      colsOptions: [
        { label: "asin", name: "ASIN", type: "text" },
        { label: "title", name: "Nome", type: "text" },
        { label: "prezzo", name: "Prezzo", type: "numeric" },
        { label: "quantita", name: "Quantità", type: "numeric" }
        //{ label: "Brand", name: "Brand", type: "text" }
      ],
    }
  
  
  }

  EseguiFattura(){
    this.acquistiService.firma(this.informazioni[4].value).subscribe((res) => {
      this.mostraDialog= false;
    })
  }

}
