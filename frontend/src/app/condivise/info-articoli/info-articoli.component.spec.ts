import { ComponentFixture, TestBed } from '@angular/core/testing';

import { InfoArticoliComponent } from './info-articoli.component';

describe('InfoArticoliComponent', () => {
  let component: InfoArticoliComponent;
  let fixture: ComponentFixture<InfoArticoliComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ InfoArticoliComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(InfoArticoliComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
