import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { ArticoliService } from '../../../service/articoli.service';

@Component({
  selector: 'app-info-articoli',
  templateUrl: './info-articoli.component.html',
  styleUrls: ['./info-articoli.component.scss']
})
export class InfoArticoliComponent implements OnInit {
  @Input() infoSelect: any[];
  @Input() nominativo: any;
  @Input() index: any;
  @Input() tipo: string;
  @Input() nuovo: boolean;
  nomef: any;
  fornitori: any[];
  tipotab: any;
  valori: any[];
  laberr="";
  public mostraDialog: boolean = true;

  @Output() Update: EventEmitter<any> = new EventEmitter<any>();
  @Output() Inserisci: EventEmitter<any> = new EventEmitter<any>();
  @Output() onHideDialog: EventEmitter<any> = new EventEmitter<any>();
  constructor(public articoliService : ArticoliService) { }

  ngOnInit(): void {
    console.log( this.nuovo)
    console.log( this.infoSelect)
    console.log( this.tipo)
    if(this.tipo=="acquisto"){
       this.nomef = {name: this.infoSelect[0].value, id: 1}
      
      this.fornitori=[
        {name: 'Paolo', id: 1},
        {name: 'Pietro', id: 2},
        {name: 'London', id: 3}
      ]
      console.log(this.fornitori)
      

    }
    
  }

  reset() {
    this.onHideDialog.emit();
  }

  nascondi() {
    this.mostraDialog = false;
    this.laberr="";
  }

  Modifica(){

    var fai=true;
    this.infoSelect.forEach(element => {
     
      if(element.value=="")
      {
        fai=false;
      }
      
    });
    if(fai){

      
    }else{
      this.laberr="Alcuni valori dei campi sono mancanti";
    }
    
    this.Update.emit({ 
      "ItemID": this.infoSelect[0].value, 
      "asin": this.infoSelect[1].value,
      "title": this.infoSelect[2].value,
      "categoria": this.infoSelect[3].value,
      "prezzo": this.infoSelect[4].value,
      "giacenza": this.infoSelect[5].value,
      "brand": this.infoSelect[6].value,
      "sogliadisponibilita": this.infoSelect[7].value
    });
    this.mostraDialog=false

  }

  Aggiungi(){
    var fai=true;
    this.infoSelect.forEach(element => {
     
      if(element.value=="")
      {
        fai=false;
      }
      
    });
    if(fai){
      
      this.Inserisci.emit({ 
        "asin": this.infoSelect[0].value,
        "title": this.infoSelect[1].value,
        "categoria": this.infoSelect[2].value,
        "prezzo": this.infoSelect[3].value,
        "giacenza": this.infoSelect[4].value,
        "brand": this.infoSelect[5].value,
        "sogliadisponibilita": this.infoSelect[6].value
      });
      this.mostraDialog=false
    }
    else{
      this.laberr="Alcuni valori dei campi sono mancanti";
    }
    
    
    
  }

}
