import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-add-articoli',
  templateUrl: './add-articoli.component.html',
  styleUrls: ['./add-articoli.component.scss']
})
export class AddArticoliComponent implements OnInit {
  @Input() infoSelect: any[];
  @Input() index: any;
  tipotab: any;
  valori: any[];
  
  @Output() onHideDialog: EventEmitter<any> = new EventEmitter<any>();
  public mostraDialog: boolean = true;

  constructor() { }

  ngOnInit(): void {
    console.log( this.infoSelect)
  }

  reset() {
    this.onHideDialog.emit();
  }
  Aggiungi(){

    console.log( this.infoSelect)
    console.log({ 
      "ASIN": this.infoSelect[0].value,
      "Title": this.infoSelect[1].value,
      "Categoria": this.infoSelect[2].value,
      "Prezzo": this.infoSelect[3].value,
      "Giacenza": this.infoSelect[4].value,
      "Brand": this.infoSelect[5].value,
      "Soglia_Disponibilità": this.infoSelect[6].value
    })
  }
}
