import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AddArticoliComponent } from './add-articoli.component';

describe('AddArticoliComponent', () => {
  let component: AddArticoliComponent;
  let fixture: ComponentFixture<AddArticoliComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AddArticoliComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AddArticoliComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
