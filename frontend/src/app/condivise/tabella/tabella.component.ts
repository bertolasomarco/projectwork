import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { DataTableOptions } from '../../../api/datatable-options';

@Component({
  selector: 'tabella',
  templateUrl: './tabella.component.html',
  styleUrls: ['./tabella.component.scss']
})
export class TabellaComponent implements OnInit {
  @Input() lista: any[];
  @Input() public options: DataTableOptions;
  @Input() btnarticolo: boolean;
  @Input() btnagg: boolean;
  @Input() btnAcqArt1: boolean;
  @Input() totalRecord: number;
  @Input() btnOrd: boolean;
  @Output() onPage: EventEmitter<any> = new EventEmitter<any>();
  @Output() onFiltra: EventEmitter<any> = new EventEmitter<any>();
  @Output() onLazyLoad: EventEmitter<any> = new EventEmitter<any>();
  @Output() onSelectRows: EventEmitter<any> = new EventEmitter<any>();
  @Output() onButtonDettClick: EventEmitter<any> = new EventEmitter<any>();
  @Output() onButtonEliminaClick: EventEmitter<any> = new EventEmitter<any>();
  @Output() onButtonVediClick: EventEmitter<any> = new EventEmitter<any>();
  constructor() { }

  select(item: any, col: any) {
    this.onSelectRows.emit({ item: item, column_name: col.label });
  }

  ngOnInit() {
  }

  Dettaglio(item: any) {
    this.onButtonDettClick.emit({ item: item });
  }
  Elimina(item: any) {
    this.onButtonEliminaClick.emit({ item: item });
  }
  VediArticoli(item: any) {
    this.onButtonVediClick.emit({ item: item });
  }

  Filtrando(event){
    this.onFiltra.emit(event);
  }
  loadFunction(event){
    this.onLazyLoad.emit(event);
  }
  pageFunction(event){
    this.onPage.emit(event);
  }
}
