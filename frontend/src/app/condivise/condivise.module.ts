import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TabellaComponent } from './tabella/tabella.component';
import { TableModule } from 'primeng/table';
import { ButtonModule } from 'primeng/button';
import { InfoArticoliComponent } from './info-articoli/info-articoli.component';
import { DialogModule } from 'primeng/dialog';
import { TabViewModule } from 'primeng/tabview';
import { InputMaskModule } from 'primeng/inputmask';
import { FormsModule } from '@angular/forms';
import { AddArticoliComponent } from './add-articoli/add-articoli.component';
import { InputTextModule } from 'primeng/inputtext';
import { DropdownModule } from 'primeng/dropdown';
import { CalendarModule } from 'primeng/calendar';
import { ArticoliacquistiComponent } from './articoliacquisti/articoliacquisti.component';
import { AutoCompleteModule } from 'primeng/autocomplete';
import { InputNumberModule } from 'primeng/inputnumber';
import { TabellaArticoliComponent } from './tabella-articoli/tabella-articoli.component';
import { CheckFatturaComponent } from './check-fattura/check-fattura.component';

@NgModule({
  declarations: [TabellaComponent, InfoArticoliComponent, AddArticoliComponent, ArticoliacquistiComponent, TabellaArticoliComponent, CheckFatturaComponent],
  exports: [
    TabellaComponent, InfoArticoliComponent, AddArticoliComponent, ArticoliacquistiComponent,TabellaArticoliComponent,CheckFatturaComponent
  ],
  imports: [
    CommonModule,
    TableModule, ButtonModule, DialogModule, TabViewModule, InputMaskModule, FormsModule, InputTextModule, DropdownModule, CalendarModule, AutoCompleteModule, InputNumberModule
  ]
})
export class CondiviseModule { }
